(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [
		{name:"slot_atlas_", frames: [[752,0,570,702],[1226,704,418,730],[752,704,472,711],[1646,1397,392,636],[1646,681,366,714],[0,908,462,720],[1324,0,413,679],[0,0,750,906],[1180,1810,449,185],[0,1750,678,185],[1739,377,272,185],[947,1436,678,185],[947,1623,678,185],[680,1810,498,188],[464,1417,481,331],[1739,0,178,375],[464,908,266,392]]},
		{name:"slot_atlas_2", frames: [[0,683,115,112],[417,196,70,70],[139,292,167,236],[278,688,121,26],[278,716,121,26],[117,728,121,22],[0,0,137,290],[139,0,137,290],[278,0,137,290],[0,292,137,290],[417,0,95,96],[417,98,95,96],[139,530,137,97],[0,584,137,97],[278,589,137,97],[139,629,137,97],[308,292,137,97],[308,391,137,97],[308,490,137,97]]}
];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.chance_light = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.chara_attack = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.chara_back = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.chara_default = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.chara_dissappointed = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.chara_presure = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.chara_setup = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.chara_shot = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.lever = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.machine = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.mascot = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.maxbet_off = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.maxbet_on = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.maxbet_push = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.reel_magic_0 = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.reel_magic_1 = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.reel_magic_2 = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.reel_shadow = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.resist = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.resist_1 = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.retry = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.spin_off = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.spin_on = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.start = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.stop_off = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.stop_on = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.sugoi = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.thunder_0 = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.thunder_1 = function() {
	this.spriteSheet = ss["slot_atlas_"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.win_Confetti = function() {
	this.initialize(img.win_Confetti);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2250,906);


(lib.zugara_bar_black = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.zugara_bar_white = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.zugara_bell = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.zugara_cherry = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.zugara_replay = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.zugara_seven = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.zugara_suika = function() {
	this.spriteSheet = ss["slot_atlas_2"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.BTN_start = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{enabled:0,disabled:5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_5 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5).call(this.frame_5).wait(5));

	// レイヤー 1
	this.instance = new lib.start();
	this.instance.parent = this;
	this.instance.setTransform(-249,-94);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-249,-94,498,188);


(lib.BTN_spin = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"enabled":0,"disabled":5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_5 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5).call(this.frame_5).wait(5));

	// レイヤー 1
	this.instance = new lib.spin_on();
	this.instance.parent = this;
	this.instance.setTransform(-339,-93);

	this.instance_1 = new lib.spin_off();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-339,-93);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},5).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-339,-93,678,185);


(lib.BTN_retry = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"enabled":0,"disabled":5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_5 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5).call(this.frame_5).wait(5));

	// レイヤー 1
	this.instance = new lib.retry();
	this.instance.parent = this;
	this.instance.setTransform(-136,-92.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-136,-92.5,272,185);


(lib.BTN_regist_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.resist_1();
	this.instance.parent = this;
	this.instance.setTransform(-339,-92.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.BTN_regist_1, new cjs.Rectangle(-339,-92.5,678,185), null);


(lib.BTN_regist = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"enabled":0,"disabled":5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_5 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5).call(this.frame_5).wait(5));

	// レイヤー 1
	this.instance = new lib.resist();
	this.instance.parent = this;
	this.instance.setTransform(-224.5,-92.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({x:-224,y:-92},0).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-224.5,-92.5,449,185);


(lib.GP_zugara_suika = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.zugara_suika();
	this.instance.parent = this;
	this.instance.setTransform(-68.5,-48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-48.5,137,97);


(lib.GP_zugara_seven = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.zugara_seven();
	this.instance.parent = this;
	this.instance.setTransform(-68.5,-48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-48.5,137,97);


(lib.GP_zugara_replay = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.zugara_replay();
	this.instance.parent = this;
	this.instance.setTransform(-68.5,-48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-48.5,137,97);


(lib.GP_zugara_poinko = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.zugara_bar_white();
	this.instance.parent = this;
	this.instance.setTransform(-68.5,-48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-48.5,137,97);


(lib.GP_zugara_cherry = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.zugara_cherry();
	this.instance.parent = this;
	this.instance.setTransform(-68.5,-48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-48.5,137,97);


(lib.GP_zugara_bell = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.zugara_bell();
	this.instance.parent = this;
	this.instance.setTransform(-68.5,-48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-48.5,137,97);


(lib.GP_zugara_bar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.zugara_bar_black();
	this.instance.parent = this;
	this.instance.setTransform(-68.5,-48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-48.5,137,97);


(lib.MC_sugoi = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.sugoi();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_sugoi, new cjs.Rectangle(0,0,481,331), null);


(lib.Mc_lineNum5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{off:0,on:5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Mc_lineNum4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"off":0,"on":5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Mc_lineNum3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"off":0,"on":5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Mc_lineNum2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"off":0,"on":5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Mc_lineNum1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"off":0,"on":5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.GP_reel_shadow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.reel_shadow();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.GP_reel_shadow, new cjs.Rectangle(0,0,137,290), null);


(lib.MC_reelmask = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AqxW+MAAAgt7IVjAAMAAAAt7g");
	this.shape.setTransform(70,147,1.014,1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_reelmask, new cjs.Rectangle(0,0,140,294), null);


(lib.MC_reellflash = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AqnWRMAAAgshIVPAAMAAAAshg");
	this.shape.setTransform(68,142.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_reellflash, new cjs.Rectangle(0,0,136,285), null);


(lib.MC_reel_mgic_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.reel_magic_2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_reel_mgic_2, new cjs.Rectangle(0,0,137,290), null);


(lib.MC_reel_magic_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.reel_magic_1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_reel_magic_1, new cjs.Rectangle(0,0,137,290), null);


(lib.MC_reel_magic_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.reel_magic_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_reel_magic_0, new cjs.Rectangle(0,0,137,290), null);


(lib.Mc_placeholder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = getMCSymbolPrototype(lib.Mc_placeholder, null, null);


(lib.MC_panelmask = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Eg6qBG3MAAAiNtMB1VAAAMAAACNtgEgkCAJxMBHbAAAMAAAgsxMhHbAAAg");
	this.shape.setTransform(375.5,453.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_panelmask, new cjs.Rectangle(0,0,751,907), null);


(lib.MC_message = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2
	this.txt_message = new cjs.Text("あ", "bold 32px 'Arial'", "#FFFFFF");
	this.txt_message.name = "txt_message";
	this.txt_message.lineHeight = 34;
	this.txt_message.lineWidth = 620;
	this.txt_message.parent = this;
	this.txt_message.setTransform(20.1,19.5);

	this.timeline.addTween(cjs.Tween.get(this.txt_message).wait(1));

	// レイヤー 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(51,51,51,0.949)").s("#FFFFFF").ss(4,1,1).rr(-330,-102.05,660,204.1,20);
	this.shape.setTransform(330.2,102,0.995,1,0,0,0,0.2,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_message, new cjs.Rectangle(-0.5,-2,661,208.1), null);


(lib.MC_mask = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Eg6lBGyMAAAiNjMB1LAAAMAAACNjg");
	this.shape.setTransform(375,453);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_mask, new cjs.Rectangle(0,0,750,906), null);


(lib.Mc_lever = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_3 = function() {
		this.parent.onLeverDown();
	}
	this.frame_5 = function() {
		this.gotoAndStop(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3).call(this.frame_3).wait(2).call(this.frame_5).wait(1));

	// １
	this.instance = new lib.lever();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({y:10},0).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,70,70);


(lib.MC_dummy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = getMCSymbolPrototype(lib.MC_dummy, null, null);


(lib.MC_confetti = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.win_Confetti();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({x:-750},0).wait(2).to({x:-1500},0).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,2250,906);


(lib.GP_reel_light = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,0,0.498)").s().p("AqrHlIAAvJIVXAAIAAPJg");
	this.shape.setTransform(68.4,48.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,136.8,97);


(lib.GP_mascot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.mascot();
	this.instance.parent = this;
	this.instance.setTransform(-83.5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.GP_mascot, new cjs.Rectangle(-83.5,0,167,236), null);


(lib.GP_chara_stand = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.chara_setup();
	this.instance.parent = this;
	this.instance.setTransform(-236,-355.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-236,-355.5,462,720);


(lib.GP_chara_presure = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.chara_presure();
	this.instance.parent = this;
	this.instance.setTransform(-183,-357);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-183,-357,366,714);


(lib.GP_chancelight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.chance_light();
	this.instance.parent = this;
	this.instance.setTransform(-57.5,-56);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.GP_chancelight, new cjs.Rectangle(-57.5,-56,115,112), null);


(lib.GP_body = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.machine();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,750,906);


(lib.GP_bet_lamp = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgvAwQgUgUAAgcQAAgbAUgUQAUgUAbAAQAcAAAUAUQATAUABAbQgBAcgTAUQgUATgcABQgbgBgUgTg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.7,-6.7,13.5,13.5);


(lib.BTN_stopEach = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"enabled":0,"disabled":5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_5 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5).call(this.frame_5).wait(5));

	// レイヤー 1
	this.instance = new lib.stop_on();
	this.instance.parent = this;
	this.instance.setTransform(-64,-60);

	this.instance_1 = new lib.stop_off();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-64,-60);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},5).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-64,-60,95,96);


(lib.BTN_maxbet = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{fr_stop:0,fr_push:7});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_12 = function() {
		this.gotoAndStop("fr_stop");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(12).call(this.frame_12).wait(1));

	// レイヤー 5
	this.instance = new lib.maxbet_push();
	this.instance.parent = this;
	this.instance.setTransform(-61,-9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({_off:false},0).wait(4));

	// レイヤー 1
	this.instance_1 = new lib.maxbet_off();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-60.5,-13);

	this.instance_2 = new lib.maxbet_on();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-61,-13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1}]}).to({state:[{t:this.instance_2}]},7).to({state:[]},2).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-60.5,-13,121,26);


(lib.Mc_zugara = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(7));

	// レイヤー 1
	this.instance = new lib.GP_zugara_seven("synched",0);
	this.instance.parent = this;

	this.instance_1 = new lib.GP_zugara_bar("synched",0);
	this.instance_1.parent = this;

	this.instance_2 = new lib.GP_zugara_poinko("synched",0);
	this.instance_2.parent = this;

	this.instance_3 = new lib.GP_zugara_suika("synched",0);
	this.instance_3.parent = this;

	this.instance_4 = new lib.GP_zugara_bell("synched",0);
	this.instance_4.parent = this;

	this.instance_5 = new lib.GP_zugara_cherry("synched",0);
	this.instance_5.parent = this;

	this.instance_6 = new lib.GP_zugara_replay("synched",0);
	this.instance_6.parent = this;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-48.5,137,97);


(lib.Mc_lineRightUp = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 4
	this.instance = new lib.GP_reel_light("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(143,-103.5,1,1,0,0,0,68.4,48.5);

	this.instance_1 = new lib.GP_reel_light("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-16.9,-6.5,1,1,0,0,0,68.4,48.5);

	this.instance_2 = new lib.GP_reel_light("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-176.9,90.5,1,1,0,0,0,68.4,48.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Mc_lineRightUp, new cjs.Rectangle(-245.3,-152,456.8,291), null);


(lib.Mc_lineRightDown = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2
	this.instance = new lib.GP_reel_light("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(142.4,91.5,1,1,0,0,0,68.4,48.5);

	this.instance_1 = new lib.GP_reel_light("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-17.6,-5.5,1,1,0,0,0,68.4,48.5);

	this.instance_2 = new lib.GP_reel_light("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-177.6,-102.5,1,1,0,0,0,68.4,48.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Mc_lineRightDown, new cjs.Rectangle(-246,-151,456.8,291), null);


(lib.Mc_lineHorizontal = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 2
	this.instance = new lib.GP_reel_light("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(143.4,48.5,1,1,0,0,0,68.4,48.5);

	this.instance_1 = new lib.GP_reel_light("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-16.6,48.5,1,1,0,0,0,68.4,48.5);

	this.instance_2 = new lib.GP_reel_light("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-175.6,48.5,1,1,0,0,0,68.4,48.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Mc_lineHorizontal, new cjs.Rectangle(-244,0,455.8,97), null);


(lib.Mc_lineDisplay = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		this.initOnce();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Line5
	this.num_5 = new lib.Mc_lineNum5();
	this.num_5.parent = this;
	this.num_5.setTransform(374,345);

	this.line_5 = new lib.Mc_lineRightUp();
	this.line_5.parent = this;
	this.line_5.setTransform(375,353);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.line_5},{t:this.num_5}]}).wait(1));

	// Line4
	this.num_4 = new lib.Mc_lineNum4();
	this.num_4.parent = this;
	this.num_4.setTransform(374,345);

	this.line_4 = new lib.Mc_lineRightDown();
	this.line_4.parent = this;
	this.line_4.setTransform(375,353);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.line_4},{t:this.num_4}]}).wait(1));

	// Line3
	this.num_3 = new lib.Mc_lineNum3();
	this.num_3.parent = this;
	this.num_3.setTransform(374,439);

	this.line_3 = new lib.Mc_lineHorizontal();
	this.line_3.parent = this;
	this.line_3.setTransform(373.4,397.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.line_3},{t:this.num_3}]}).wait(1));

	// Line2
	this.num_2 = new lib.Mc_lineNum2();
	this.num_2.parent = this;
	this.num_2.setTransform(374,258);

	this.line_2 = new lib.Mc_lineHorizontal();
	this.line_2.parent = this;
	this.line_2.setTransform(374,202.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.line_2},{t:this.num_2}]}).wait(1));

	// Line1
	this.num_1 = new lib.Mc_lineNum1();
	this.num_1.parent = this;
	this.num_1.setTransform(373.4,298.1);

	this.line_1 = new lib.Mc_lineHorizontal();
	this.line_1.parent = this;
	this.line_1.setTransform(373.4,300.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.line_1},{t:this.num_1}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Mc_lineDisplay, new cjs.Rectangle(129,201,457.4,293.5), null);


(lib.MC_thunder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_5 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(5).call(this.frame_5).wait(1));

	// thunder_0.png
	this.instance = new lib.thunder_0();
	this.instance.parent = this;
	this.instance.setTransform(-88,-189);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({skewY:180,x:90,y:-130},0).wait(1).to({skewY:0,x:-88,y:0},0).to({_off:true},2).wait(2));

	// thunder_1.png
	this.instance_1 = new lib.thunder_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-133,167);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({_off:true},2).wait(2));

	// レイヤー 4
	this.instance_2 = new lib.MC_reellflash();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-1.2,401.7,1,1,0,0,0,70,147);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).wait(2).to({alpha:0.5},0).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88,-189,178,375);


(lib.MC_message_2 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_8 = function() {
		this.stop();
	}
	this.frame_11 = function() {
		this.gotoAndPlay(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(8).call(this.frame_8).wait(3).call(this.frame_11).wait(1));

	// レイヤー 1
	this.mc_message = new lib.MC_message();
	this.mc_message.parent = this;
	this.mc_message.setTransform(1074.2,67,1,1,0,0,0,330,67);

	this.timeline.addTween(cjs.Tween.get(this.mc_message).to({x:320},7,cjs.Ease.get(-1)).wait(1).to({x:330},0).to({x:-406.1},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(743.7,-2,661,208.1);


(lib.MC_mascot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_18 = function() {
		this.gotoAndPlay(0);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

	// レイヤー 1
	this.instance = new lib.GP_mascot();
	this.instance.parent = this;
	this.instance.setTransform(0,118,1,1,0,0,0,0,118);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:103},9).to({y:118},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-83.5,0,167,236);


(lib.MC_chara = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{fr_default:0,"fr_stop":10,fr_standby:20,fr_shot:34,fr_back:44,fr_dissappointed:55,fr_presure:64,fr_attack:76});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_10 = function() {
		this.stop();
	}
	this.frame_33 = function() {
		this.gotoAndPlay("fr_standby");
	}
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_44 = function() {
		this.stop();
	}
	this.frame_55 = function() {
		this.stop();
	}
	this.frame_75 = function() {
		this.gotoAndPlay("fr_presure")
	}
	this.frame_76 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10).call(this.frame_10).wait(23).call(this.frame_33).wait(6).call(this.frame_39).wait(5).call(this.frame_44).wait(11).call(this.frame_55).wait(20).call(this.frame_75).wait(1).call(this.frame_76).wait(14));

	// レイヤー 3
	this.instance = new lib.chara_default();
	this.instance.parent = this;

	this.instance_1 = new lib.GP_chara_stand("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(236,355.5);
	this.instance_1._off = true;

	this.instance_2 = new lib.chara_setup();
	this.instance_2.parent = this;

	this.instance_3 = new lib.chara_shot();
	this.instance_3.parent = this;

	this.instance_4 = new lib.chara_back();
	this.instance_4.parent = this;

	this.instance_5 = new lib.chara_dissappointed();
	this.instance_5.parent = this;

	this.instance_6 = new lib.GP_chara_presure("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(183,357);
	this.instance_6._off = true;

	this.instance_7 = new lib.chara_attack();
	this.instance_7.parent = this;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},10).to({state:[{t:this.instance_1}]},10).to({state:[{t:this.instance_1}]},7).to({state:[{t:this.instance_1}]},6).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},8).to({state:[{t:this.instance_5}]},11).to({state:[{t:this.instance_6}]},9).to({state:[{t:this.instance_6}]},6).to({state:[{t:this.instance_6}]},5).to({state:[{t:this.instance_7}]},1).wait(14));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10).to({_off:false},0).wait(10).to({startPosition:0},0).to({y:331.5},7).to({y:355.5},6).to({_off:true},1).wait(56));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(64).to({_off:false},0).to({y:189},6,cjs.Ease.get(1)).to({y:357},5,cjs.Ease.get(-1)).to({_off:true,x:0,y:0},1,cjs.Ease.get(1)).wait(14));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,472,711);


(lib.MC_chancelight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.GP_chancelight();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_chancelight, new cjs.Rectangle(-57.5,-56,115,112), null);


(lib.MC_bet_lamp = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.instance = new lib.GP_bet_lamp("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.MC_bet_lamp, new cjs.Rectangle(-6.7,-6.7,13.5,13.5), null);


(lib.MC_betlamp = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{fr_Off:0,fr_On:5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_9 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(9).call(this.frame_9).wait(1));

	// mc_bet_lamp_1
	this.mc_bet_lamp_1 = new lib.MC_bet_lamp();
	this.mc_bet_lamp_1.parent = this;
	this.mc_bet_lamp_1.setTransform(6.8,53.6);
	this.mc_bet_lamp_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc_bet_lamp_1).wait(5).to({_off:false},0).wait(5));

	// mc_bet_lamp_2
	this.mc_bet_lamp_2 = new lib.MC_bet_lamp();
	this.mc_bet_lamp_2.parent = this;
	this.mc_bet_lamp_2.setTransform(6.8,29.9);
	this.mc_bet_lamp_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc_bet_lamp_2).wait(6).to({_off:false},0).wait(4));

	// mc_bet_lamp_3
	this.mc_bet_lamp_3 = new lib.MC_bet_lamp();
	this.mc_bet_lamp_3.parent = this;
	this.mc_bet_lamp_3.setTransform(6.8,6.8);
	this.mc_bet_lamp_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc_bet_lamp_3).wait(7).to({_off:false},0).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Mc_game = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		// slot_mc_game.js に大部分のスクリプトを書いてある
		// 次フレームで初期化する
	}
	this.frame_1 = function() {
		this.stop();
		this.initOnce(); // 一度だけの初期化
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1));

	// MC_mask
	this.instance = new lib.MC_mask();
	this.instance.parent = this;
	this.instance.setTransform(375,453,1,1,0,0,0,375,453);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},1).wait(1));

	// キャラ
	this.mc_chara = new lib.MC_chara();
	this.mc_chara.parent = this;
	this.mc_chara.setTransform(650.1,387,0.5,0.5,0,0,0,236.1,355.5);

	this.timeline.addTween(cjs.Tween.get(this.mc_chara).wait(2));

	// スタートボタン
	this.btn_start = new lib.BTN_start();
	this.btn_start.parent = this;
	this.btn_start.setTransform(395,770.1);

	this.timeline.addTween(cjs.Tween.get(this.btn_start).wait(2));

	// 登録ボタン/リトライボタン
	this.btn_regist_1 = new lib.BTN_regist_1();
	this.btn_regist_1.parent = this;
	this.btn_regist_1.setTransform(382,770.5);

	this.btn_retry = new lib.BTN_retry();
	this.btn_retry.parent = this;
	this.btn_retry.setTransform(585,770.5);

	this.btn_regist = new lib.BTN_regist();
	this.btn_regist.parent = this;
	this.btn_regist.setTransform(266.5,770.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.btn_regist},{t:this.btn_retry},{t:this.btn_regist_1}]}).wait(2));

	// すごーい
	this.mc_sugoi = new lib.MC_sugoi();
	this.mc_sugoi.parent = this;
	this.mc_sugoi.setTransform(519.5,137,1,1,0,0,0,186.5,153);

	this.timeline.addTween(cjs.Tween.get(this.mc_sugoi).wait(2));

	// textLayer
	this.mc_textLayer = new lib.MC_dummy();
	this.mc_textLayer.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.mc_textLayer).wait(2));

	// キャラ2
	this.mc_chara2 = new lib.MC_chara();
	this.mc_chara2.parent = this;
	this.mc_chara2.setTransform(1105.2,760.2,1.5,1.5,0,0,0,236.1,355.5);

	this.timeline.addTween(cjs.Tween.get(this.mc_chara2).wait(2));

	// マスコット
	this.mc_mascot = new lib.MC_mascot();
	this.mc_mascot.parent = this;
	this.mc_mascot.setTransform(637,225.7,1.5,1.5,0,0,0,0,118);

	this.timeline.addTween(cjs.Tween.get(this.mc_mascot).wait(2));

	// 個別STOPボタン
	this.mc_btn1 = new lib.BTN_stopEach();
	this.mc_btn1.parent = this;
	this.mc_btn1.setTransform(392.9,670.1,1,1.003);

	this.mc_btn2 = new lib.BTN_stopEach();
	this.mc_btn2.parent = this;
	this.mc_btn2.setTransform(530.6,668.2,1,1.003);

	this.mc_btn0 = new lib.BTN_stopEach();
	this.mc_btn0.parent = this;
	this.mc_btn0.setTransform(250.7,669,1,1.003);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_btn0},{t:this.mc_btn2},{t:this.mc_btn1}]}).wait(2));

	// STOPボタン
	this.btn_stop = new lib.MC_mask();
	this.btn_stop.parent = this;
	this.btn_stop.setTransform(375,453,1,1,0,0,0,375,453);
	this.btn_stop.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.btn_stop).wait(2));

	// SPINボタン
	this.btn_spin = new lib.BTN_spin();
	this.btn_spin.parent = this;
	this.btn_spin.setTransform(383,823);

	this.timeline.addTween(cjs.Tween.get(this.btn_spin).wait(2));

	// ダミーボタンレイヤー
	this.btn_dummy = new lib.MC_dummy();
	this.btn_dummy.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.btn_dummy).wait(2));

	// chance
	this.mc_chancelight = new lib.MC_chancelight();
	this.mc_chancelight.parent = this;
	this.mc_chancelight.setTransform(64.5,293);

	this.timeline.addTween(cjs.Tween.get(this.mc_chancelight).wait(2));

	// cnfetti
	this.mc_confetti = new lib.MC_confetti();
	this.mc_confetti.parent = this;
	this.mc_confetti.setTransform(1125,453,1,1,0,0,0,1125,453);

	this.timeline.addTween(cjs.Tween.get(this.mc_confetti).wait(2));

	// reelmagic
	this.mc_reel_magic_2 = new lib.MC_reel_mgic_2();
	this.mc_reel_magic_2.parent = this;
	this.mc_reel_magic_2.setTransform(533.3,375,1,1,0,0,0,68.5,145);

	this.mc_reel_magic_1 = new lib.MC_reel_magic_1();
	this.mc_reel_magic_1.parent = this;
	this.mc_reel_magic_1.setTransform(373.3,374.8,1,1,0,0,0,68.5,145);

	this.mc_reel_magic_0 = new lib.MC_reel_magic_0();
	this.mc_reel_magic_0.parent = this;
	this.mc_reel_magic_0.setTransform(213.3,375,1,1,0,0,0,68.5,145);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_reel_magic_0},{t:this.mc_reel_magic_1},{t:this.mc_reel_magic_2}]}).wait(2));

	// thunder
	this.mc_thunder_2 = new lib.MC_thunder();
	this.mc_thunder_2.parent = this;
	this.mc_thunder_2.setTransform(537,255.5,1,1,0,0,0,0,279.5);

	this.mc_thunder_1 = new lib.MC_thunder();
	this.mc_thunder_1.parent = this;
	this.mc_thunder_1.setTransform(376,255.5,1,1,0,0,0,0,279.5);

	this.mc_thunder_0 = new lib.MC_thunder();
	this.mc_thunder_0.parent = this;
	this.mc_thunder_0.setTransform(217,255.5,1,1,0,0,0,0,279.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_thunder_0},{t:this.mc_thunder_1},{t:this.mc_thunder_2}]}).wait(2));

	// effectLayer
	this.mc_effectLayer = new lib.MC_dummy();
	this.mc_effectLayer.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.mc_effectLayer).wait(2));

	// message
	this.mc_message_2 = new lib.MC_message_2();
	this.mc_message_2.parent = this;
	this.mc_message_2.setTransform(375,381,1,1,0,0,0,330,67);

	this.timeline.addTween(cjs.Tween.get(this.mc_message_2).wait(2));

	// reelmask
	this.mc_reelmask_2 = new lib.MC_reelmask();
	this.mc_reelmask_2.parent = this;
	this.mc_reelmask_2.setTransform(531.9,376.2,1,1,0,0,0,68.8,146.8);
	this.mc_reelmask_2.alpha = 0.602;

	this.mc_reelmask_1 = new lib.MC_reelmask();
	this.mc_reelmask_1.parent = this;
	this.mc_reelmask_1.setTransform(372.4,376.2,1,1,0,0,0,68.8,146.8);
	this.mc_reelmask_1.alpha = 0.602;

	this.mc_reelmask_0 = new lib.MC_reelmask();
	this.mc_reelmask_0.parent = this;
	this.mc_reelmask_0.setTransform(213.5,375.8,1,1,0,0,0,68.8,146.8);
	this.mc_reelmask_0.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_reelmask_0},{t:this.mc_reelmask_1},{t:this.mc_reelmask_2}]}).wait(2));

	// panelmask
	this.mc_panelmask = new lib.MC_panelmask();
	this.mc_panelmask.parent = this;
	this.mc_panelmask.setTransform(375.5,453.5,1,1,0,0,0,375.5,453.5);
	this.mc_panelmask.alpha = 0.602;

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AuMW8I1hAAMAAAgt3IVhAAMAx6AAAMAAAAt3gAuM27MAAAAt3");
	this.shape.setTransform(373.4,376.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.mc_panelmask}]}).wait(2));

	// Mask
	this.mc_effectmask = new lib.MC_mask();
	this.mc_effectmask.parent = this;
	this.mc_effectmask.setTransform(375,429,1,1,0,0,0,375,429);
	this.mc_effectmask.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.mc_effectmask).wait(2));

	// CREDIT　WIN
	this.mc_win_num = new lib.Mc_placeholder();
	this.mc_win_num.parent = this;
	this.mc_win_num.setTransform(100.7,402.7);

	this.mc_credit_num = new lib.Mc_placeholder();
	this.mc_credit_num.parent = this;
	this.mc_credit_num.setTransform(100.7,487);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_credit_num},{t:this.mc_win_num}]}).wait(2));

	// MAXBET
	this.btn_maxbet = new lib.BTN_maxbet();
	this.btn_maxbet.parent = this;
	this.btn_maxbet.setTransform(117.5,553);

	this.timeline.addTween(cjs.Tween.get(this.btn_maxbet).wait(2));

	// レバー
	this.mc_lever = new lib.Mc_lever();
	this.mc_lever.parent = this;
	this.mc_lever.setTransform(21.3,608);

	this.timeline.addTween(cjs.Tween.get(this.mc_lever).wait(2));

	// メダルランプ
	this.mc_betlamp = new lib.MC_betlamp();
	this.mc_betlamp.parent = this;
	this.mc_betlamp.setTransform(645.2,462.4,1,1,0,0,0,6.8,30.2);

	this.timeline.addTween(cjs.Tween.get(this.mc_betlamp).wait(2));

	// 筺体
	this.instance_1 = new lib.GP_body("synched",0);
	this.instance_1.parent = this;

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(1,1,1).p("ABQAAQAAAhgXAYQgYAYghAAQghAAgXgYQgYgYAAghQAAghAYgXQAXgYAhAAQAhAAAYAYQAXAXAAAhg");
	this.shape_1.setTransform(698.2,432.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,255,0,0.498)").s().p("Ag4A5QgYgYABghQgBghAYgXQAYgYAgAAQAiAAAXAYQAXAXABAhQgBAhgXAYQgXAYgiAAQggAAgYgYg");
	this.shape_2.setTransform(698.2,432.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.instance_1}]}).wait(2));

	// ライン表示
	this.mc_lineDisplay = new lib.Mc_lineDisplay();
	this.mc_lineDisplay.parent = this;
	this.mc_lineDisplay.setTransform(391,457,1,1,0,0,0,375,429);

	this.timeline.addTween(cjs.Tween.get(this.mc_lineDisplay).wait(2));

	// リール影
	this.instance_2 = new lib.GP_reel_shadow();
	this.instance_2.parent = this;
	this.instance_2.setTransform(214.5,375,1,1,0,0,0,68.5,145);

	this.instance_3 = new lib.reel_shadow();
	this.instance_3.parent = this;
	this.instance_3.setTransform(466,230);

	this.instance_4 = new lib.reel_shadow();
	this.instance_4.parent = this;
	this.instance_4.setTransform(306,230);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4},{t:this.instance_3},{t:this.instance_2}]}).wait(2));

	// リール図柄
	this.mc_reel2_3 = new lib.Mc_zugara();
	this.mc_reel2_3.parent = this;
	this.mc_reel2_3.setTransform(534,565);

	this.mc_reel2_2 = new lib.Mc_zugara();
	this.mc_reel2_2.parent = this;
	this.mc_reel2_2.setTransform(534,471);

	this.mc_reel2_1 = new lib.Mc_zugara();
	this.mc_reel2_1.parent = this;
	this.mc_reel2_1.setTransform(534,375);

	this.mc_reel2_0 = new lib.Mc_zugara();
	this.mc_reel2_0.parent = this;
	this.mc_reel2_0.setTransform(534,279);

	this.mc_reel1_3 = new lib.Mc_zugara();
	this.mc_reel1_3.parent = this;
	this.mc_reel1_3.setTransform(374,565);

	this.mc_reel1_2 = new lib.Mc_zugara();
	this.mc_reel1_2.parent = this;
	this.mc_reel1_2.setTransform(374,471);

	this.mc_reel1_1 = new lib.Mc_zugara();
	this.mc_reel1_1.parent = this;
	this.mc_reel1_1.setTransform(374,375);

	this.mc_reel1_0 = new lib.Mc_zugara();
	this.mc_reel1_0.parent = this;
	this.mc_reel1_0.setTransform(374,279);

	this.mc_reel0_3 = new lib.Mc_zugara();
	this.mc_reel0_3.parent = this;
	this.mc_reel0_3.setTransform(214,565);

	this.mc_reel0_2 = new lib.Mc_zugara();
	this.mc_reel0_2.parent = this;
	this.mc_reel0_2.setTransform(214,471);

	this.mc_reel0_1 = new lib.Mc_zugara();
	this.mc_reel0_1.parent = this;
	this.mc_reel0_1.setTransform(214,375);

	this.mc_reel0_0 = new lib.Mc_zugara();
	this.mc_reel0_0.parent = this;
	this.mc_reel0_0.setTransform(214,279);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mc_reel0_0},{t:this.mc_reel0_1},{t:this.mc_reel0_2},{t:this.mc_reel0_3},{t:this.mc_reel1_0},{t:this.mc_reel1_1},{t:this.mc_reel1_2},{t:this.mc_reel1_3},{t:this.mc_reel2_0},{t:this.mc_reel2_1},{t:this.mc_reel2_2},{t:this.mc_reel2_3}]}).wait(2));

	// リールバックの黒
	this.instance_5 = new lib.MC_mask();
	this.instance_5.parent = this;
	this.instance_5.setTransform(376.1,374.1,0.624,0.329,0,0,0,375.4,453.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-213,2250,1506.5);


// stage content:
(lib.slot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{fr_Init:0,fr_Game:5});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		
		//----------------------------------------
		//以下全ゲーム共通
		//----------------------------------------
		
		this.addChild(cf.stage);
		this.addChild(cf.error_dialog);
		this.addChild(cf.connect_error_dialog);
		this.addChild(cf.mask);
		this.addChild(cf.loading);
		cf.mask.visible = false;
		
		//API初期化
		sf.apiInit();
		//----------------------------------------
	}
	this.frame_5 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5).call(this.frame_5).wait(11));

	// Mask
	this.instance = new lib.MC_mask();
	this.instance.parent = this;
	this.instance.setTransform(375,453,1,1,0,0,0,375,453);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},5).wait(11));

	// Main
	this.mc_game = new lib.Mc_game();
	this.mc_game.parent = this;
	this.mc_game.setTransform(0.6,0.1,1,1,0,0,0,0.6,0.1);
	this.mc_game._off = true;

	this.timeline.addTween(cjs.Tween.get(this.mc_game).wait(5).to({_off:false},0).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(375,453,750,906);
// library properties:
lib.properties = {
	width: 750,
	height: 906,
	fps: 15,
	color: "#666666",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/win_Confetti.png", id:"win_Confetti"},
		{src:"images/slot_atlas_.png", id:"slot_atlas_"},
		{src:"images/slot_atlas_2.png", id:"slot_atlas_2"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;