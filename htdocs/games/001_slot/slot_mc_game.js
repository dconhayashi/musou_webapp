(function (lib, cjs) {
	// ---------------------------------
	// 定数
	// ---------------------------------
	var DEBUG = false;
	var FREEZE_DEBUG = false; // STOPボタン連打でフリーズテスト用
	var REEL_CHECK_DEBUG = false; // リールの全出目重複成立チェックデバッグ
	
	// 図柄ID: Mc_zugaraにおけるフレーム番号とする
	var ZGR_SEVEN = 0;
	var ZGR_BAR = 1;
	var ZGR_BAR2 = 2;
	var ZGR_SUIKA = 3;
	var ZGR_BELL = 4;
	var ZGR_CHERRY = 5;
	var ZGR_REPLAY = 6;
	var HAZURE = 7; // 図柄ではないがハズレ役IDとして

	// 役の種類
	var ZGR_MAX = 7; // 全図柄
	var YAKU_MAX = ZGR_MAX + 1; // 全図柄＋ハズレ

	// 図柄IDごとの倍率
	var MAGNIFICATION = [
		70, // POINKO
		35, // SEVEN
		21, // SUIKA
		7, // BELL
		7, // BAR
		3, // CHERRY
		1, // REPLAY
		0 // HAZURE
	];

	// リールのコマ数
	var MAX_KOMA= 21;

	// リール配列
	var REEL = [
		[
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_SUIKA,
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_SUIKA,
			ZGR_CHERRY,
			ZGR_SUIKA,
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_BAR2, // 10
			ZGR_CHERRY,
			ZGR_SUIKA,
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_SEVEN,
			ZGR_BAR,
			ZGR_SEVEN,
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_SUIKA,
		],
		[
			ZGR_REPLAY,
			ZGR_BELL,
			ZGR_SUIKA,
			ZGR_REPLAY,
			ZGR_SUIKA,
			ZGR_REPLAY,
			ZGR_BELL,
			ZGR_SUIKA,
			ZGR_CHERRY,
			ZGR_BAR2, // 9
			ZGR_REPLAY,
			ZGR_BELL,
			ZGR_SUIKA,
			ZGR_REPLAY,
			ZGR_BELL,
			ZGR_SUIKA,
			ZGR_BAR,
			ZGR_SEVEN,
			ZGR_REPLAY,
			ZGR_BELL,
			ZGR_SUIKA,
		],
		[
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_CHERRY,
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_BELL,
			ZGR_BAR2, // 6
			ZGR_SUIKA,
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_CHERRY,
			ZGR_CHERRY,
			ZGR_CHERRY,
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_SEVEN,
			ZGR_BAR,
			ZGR_SUIKA,
			ZGR_BELL,
			ZGR_REPLAY,
			ZGR_CHERRY,
		]
	];

	var MAX_LINE = 5;

	// ---------------------------------
	// 変数
	// ---------------------------------
	
	var p = lib.Mc_game.prototype;
	var _this = null; // インスタンスを実行時に保持

	// 画像文字インスタンス
	var imgNumCredit = new cjs.ImageNumber("images/suuji.png",25,38,{align:"right", max:999});
	var imgNumWin = new cjs.ImageNumber("images/suuji.png",25,38,{align:"right", max:999});
	var imgNumLeft = new cjs.ImageNumber("images/bold_number.png",35,52,{align:"center", max:99, default:0, baseX:75, baseY:-27})
	var imgNumRetry = new cjs.ImageNumber("images/bold_number.png",35,52,{align:"center", max:99, default:0})
	//ゲットメダル用の文字
	var medal_get_text = new sf.MedalGetText({});
	var medal_total_text　= new sf.MedalTotalText({});

	// 所持メダル
	//var userMedal = 100; // 通信対応のため sf.userMedal に移行済み

	// ベット枚数（ライン数乗算前）
	var betMedal = 1;

	// ベットライン数
	var betLine = 5;

	// 獲得メダル
	var getMedal = 0;

	// スロット変数
	// 2016-12-01 リール回転をなめらかにするため、komaIdxに小数を許容するように変更
	var komaIdxArray = [10.0, 9.0, 6.0]; // 各リールのコマインデックス配列 ポインコで初期化
	var btnPressedCount = 0; // STOPボタン押された回数
	var hitYaku; // 当選役
	var hitLine; // 当選ライン
	var hazureKomaIdxArray = []; // ハズレkomaIdxパターン
	//var hazureZugaraArray = []; // ハズレ図柄パターン
	var btnPressedArray = []; // ボタンプレスフラグ
	var reelStoppedArray = []; //リールストップフラグ

	// リールスピード（単位：コマ）
	// 小数で指定できる。
	// プラスで順回転、マイナスで逆回転
	var reelSpeed = 0.75;

	// リール停止パラメータ
	var reelStopDuration = 300;
	var reelStopEase = cjs.Ease.linear;
	var reelStopEase = cjs.Ease.backOut;
	// ペカリフラグ
	var pekaFlag = false;
	//　ポインコ振動フラグ
	var shakeFlag = 0;

	var stopIdx = -1;
	// ---------------------------------
	// 関数
	// ---------------------------------

	// 一度だけの初期化
	p.initOnce = function() { // frame_0
		_this = this;

		if (REEL_CHECK_DEBUG) {
			reelCheck();
		}

		// リール表示初期化
		for(var reelIdx = 0; reelIdx < 3; reelIdx++){
			var komaIdx = komaIdxArray[reelIdx];
			this.fr_reelSet(reelIdx, komaIdx);
		}

		//液晶画面初期化
		this.mc_sugoi.visible = false;
		this.mc_confetti.visible = false;
		this.mc_effectmask.visible = true;
		this.mc_panelmask.visible = false;
		this.btn_regist.visible = false;
		this.btn_regist_1.visible = false;
		this.btn_retry.visible = false;
		this.btn_start.visible = false;
		//this.mc_serif.visible = false;
		this.btn_spin.visible = false;
		this.mc_chara.visible = false;
		for(var i=0; i<3; i++){
			this["mc_thunder_" + i].visible = false;
			this["mc_reelmask_" + i].visible = false;
			this["mc_reel_magic_" + i].visible = false;
		}
		// SPINボタン
		this.btn_spin.addButtonFunc(apiBet);

		function apiBet(){
			_this.btn_spin.setButtonEnabled(false);
			sf.bet();
		}

		// ワンキーSTOPボタン
		this.btn_stop.addButtonFunc(this.stopReel, null, this.pushStopButton);
		this.btn_stop.visible = false;

		for(var i = 0; i < 3; i++){
			// 個別STOPボタン
			this["mc_btn" + i].addButtonFunc(this.stopReel, {reelIdx:i}, this.pushStopButton);
			this["mc_btn" + i].setButtonEnabled(false);
			// 雷
			this["mc_thunder_" + i].visible = false;
		}

		// ライン表示
		this.mc_lineDisplay.betUpTo(0);

		// 画像文字インスタンス配置
		this.mc_credit_num.addChild(imgNumCredit);
		this.mc_win_num.addChild(imgNumWin);
		this.bonusLampAction(PK_DEFAULT);

		//this.btn_pause.addButtonFunc(cf.show_menu);
		//this.btn_pause.setButtonEnabled(true);

		//メダルゲットテキスト
		this.mc_textLayer.addChild(medal_get_text);
		this.mc_textLayer.addChild(medal_total_text);
		//this.showMessage("「BET MEDAL」でゲームに\n賭けるメダルの枚数を選択し、\n「BET LINE」でメダルを賭ける\nライン数を指定してください。", 350);
		this.btn_spin.addChild(imgNumLeft);

		this.btn_retry.addButtonFunc(this.retryGame, {count:2});
		imgNumRetry.scaleX = 0.5;
		imgNumRetry.scaleY = 0.5;
		imgNumRetry.baseX = 15;
		imgNumRetry.y = 8;
		this.btn_retry.addChild(imgNumRetry);

		
		this.btn_start.addButtonFunc(this.startGame, {count:2});
		this.btn_regist.addButtonFunc(sf.regist);
		this.btn_regist_1.addButtonFunc(sf.regist);
		this.btn_maxbet.addButtonFunc(null);
		this.mc_chara2.x = 1000;

		this.mc_chara2.regX = this.mc_chara2.getBounds().width/2;
		this.mc_chara2.regY = this.mc_chara2.getBounds().height/2;

		/*
		createjs.Tween.get(this.mc_chara2)
			.to({x:550}, 500, createjs.Ease.backOut)
			.call(function(){
				//_this.mc_serif.visible = true
				_this.btn_start.visible = true;
			})
		*/
		this.mc_chara2.x = 550;
		this.mc_chara2.visible = false;
		this.mc_mascot.visible = false;
		//this.btn_start.visible = true;


		//スクロール対応
		var scroListener = new createjs.Shape();
		var hitShape = new createjs.Shape();
		hitShape.graphics.beginFill("rgba(0,0,0,1)");
		hitShape.graphics.drawRect(0,0,lib.properties.width, lib.properties.height);
		scroListener.hitArea = hitShape;
		scroListener.on("mousedown", cf.scrollStart);
		scroListener.on("pressmove", cf.scrollMove);
		scroListener.on("mouseup", cf.scrollEnd);
		this.btn_dummy.addChild(scroListener);
		

		sf.apiBet();

	}

	//ゲーム開始
	p.startGame = function(){
		//sf.apiBet();
		//_this.mc_serif.visible = false
		_this.btn_start.visible = false;

		_this.mc_chara2.gotoAndStop("fr_back");
		_this.mc_mascot.scaleX = -1;

		createjs.Tween.get(_this.mc_chara2)
			.to({x:1000}, 1500, createjs.Ease.backOut)
			.call(function(){
			})
		createjs.Tween.get(_this.mc_mascot)
			.to({y:-300}, 1000, createjs.Ease.backOut)
			.call(function(){
				sf.apiBet();
			})
	}

	// 毎ゲームの初期化
	p.initGame = function(){
		//毎回やる初期化
		btnPressedCount = 0;
		//trace("もろもろ初期化");
		for(var i = 0; i < 3; i++){
			// 個別STOPボタン消灯
			this.setEachStopBtnEnabled(i, false);
			// ボタンプレスフラグ
			btnPressedArray[i] = false;
			// リールストップフラグ
			reelStoppedArray[i] = false;
		}


		this.btn_stop.visible = false;
		this.mc_effectmask.visible = false;
	
		// ライン表示
		this.mc_lineDisplay.setLineVisible(true);
		this.mc_lineDisplay.stopBlink();
		
		for(var i=0; i<3; i++){
			this["mc_thunder_" + i].visible = false;
			this["mc_reelmask_" + i].visible = false;
			this["mc_reel_magic_" + i].visible = false;
		}
		this.mc_panelmask.visible = false;
	 	this.mc_chara.visible = false;
	 	this.mc_chara.x = 1000;
		this.mc_chara.gotoAndStop("fr_default");

		imgNumWin.setNumber(0);
		imgNumLeft.setNumber(sf.gameLeft);
		pekaFlag = false;
		this.bonusLampAction(PK_DEFAULT);
		imgNumCredit.setNumber(sf.creditMedal);
		var counter=0;
		cjs.Ticker.on("tick", tickHandler);
		function tickHandler(event){
			if (event.paused) return;
			counter++;
			switch(counter){
				case 1:
					_this.btn_maxbet.gotoAndPlay("fr_push");
					break;
				case 3:
					if(sf.hitYaku != 6) sf.creditMedal -=3;
					imgNumCredit.setNumber(sf.creditMedal);
					_this.mc_betlamp.gotoAndPlay("fr_On");
					break;
				case 7:
					// ボタン押せるかチェック
					event.remove();
					_this.btn_spin.visible = true;
					_this.checkButtons();
					break;
				default:
					break;

			}

		}
	}

	// ゲーム終了
	p.endGame = function(){
		this.mc_lineDisplay.stopBlink();
		this.bonusLampAction(PK_DEFAULT);
		this.mc_chara.visible = false;
		this.btn_spin.visible = false;
		for(var i=0; i<3; i++){
			this["mc_thunder_" + i].visible = false;
			this["mc_reelmask_" + i].visible = false;
			this["mc_reel_magic_" + i].visible = false;
		}

		//トータル表示
		this.mc_effectmask.visible = true;
		medal_total_text.show(sf.creditMedal, 100, showButton);

		function chara2Move(){
			_this.mc_confetti.visible = true;
			_this.mc_chara2.gotoAndStop("fr_attack");
			_this.mc_chara2.x = -200;
			_this.mc_chara2.y = 1100;
			_this.mc_chara2.rotation = -50;
			_this.mc_chara2.visible =true;
			_this.mc_sugoi.scaleX = 0.1;
			_this.mc_sugoi.scaleY = 0.1;

			createjs.Tween.get(_this.mc_chara2)
				.to({x:280, y:500}, 750, createjs.Ease.backOut)
				.call(function(){
					_this.mc_sugoi.visible = true
					createjs.Tween.get(_this.mc_sugoi)
						.to({scaleX:1, scaleY:1}, 200, createjs.Ease.backOut)
				});
		}

		function showButton(){
			if(sf.creditMedal > sf.presantMedal){
				chara2Move();
			}
			if(sf.retryLeft < 1){
				_this.btn_regist_1.visible = true
			}else{
				imgNumRetry.setNumber(sf.retryLeft);
				_this.btn_regist.visible = true;
				_this.btn_retry.visible = true;
			}
		}
	}

	//再プレイ
	p.retryGame = function(){
		createjs.Tween.removeTweens(_this.mc_chara2);
		medal_total_text.visible = false;
		_this.mc_confetti.visible = false;
		_this.btn_regist.visible = false;
		_this.btn_retry.visible = false;
		_this.mc_sugoi.visible = false;
		_this.mc_chara2.visible = false;
		sf.apiBet();
	}

	// リールの全出目チェック
	function reelCheck() {
		var hitCounts = [0, 0, 0, 0, 0, 0]; // 同時成立0〜5になっている出目数をカウント
		//console.log(" 2ライン以上成立している出目 START");
		// ki: KomaIdx
		// ki0, ki1, ki2: 左、中、右のKomaIdx
		for(var ki0 = 0; ki0 < MAX_KOMA; ki0++){ // 左ループ
			komaIdxArray[0] = ki0;

			for(var ki1 = 0; ki1 < MAX_KOMA; ki1++){ // 中ループ
				komaIdxArray[1] = ki1;

				for(var ki2 = 0; ki2 < MAX_KOMA; ki2++){ // 右ループ
					komaIdxArray[2] = ki2;

					// 重複成立チェック
					// 成立ライン数を数える
					var hitCount = countHitLine(komaIdxArray);

					// 2以上だったら出力
					//if (hitCount >= 2) {
						//console.log("  ki0 ki1 ki2:", ki0, ki1, ki2, ", hitCount:", hitCount);
					//}

					// その成立ライン数出目カウントをアップ
					hitCounts[hitCount]++;
				}
				//break; // tmp
			}
			//break; // tmp
		}

		//console.log(" 2ライン以上成立している出目 END");

		// チェック結果を出力
		var sum = 0;
		for (var cnt=0; cnt<=5; cnt++) {
			//console.log("  同時成立数", cnt, "の出目数:", hitCounts[cnt]);
			sum += hitCounts[cnt];
		}
		//console.log("  合計", sum);
		//console.log("  MAX_KOMAの3乗", Math.pow(MAX_KOMA, 3));
		//console.log(" チェック結果 END");
		//console.log("reelCheck END");
	}
	// 指定リール状態のヒットライン数を調べる
	// targetKomaIdxArray: 調べたいリール状態の、ライン1のkomaIdxの配列
	function countHitLine(targetKomaIdxArray) {
		var hitCount = 0;
		var targetKomaIdx = [];
		for (var line = 1; line <= 5; line++) {
			for (var reelIdx = 0; reelIdx < 3; reelIdx++) {
				targetKomaIdx[reelIdx] = normalizeKomaIdx( targetKomaIdxArray[reelIdx] + HITLINE_OFFSET[line-1][reelIdx] );
			}

			if (
				REEL[0][targetKomaIdx[0]] == REEL[1][targetKomaIdx[1]] &&
				REEL[1][targetKomaIdx[1]] == REEL[2][targetKomaIdx[2]]
			)
			{
				//console.log("line:", line, "zgr:", REEL[0][targetKomaIdx[0]]);
				//console.log("  ", REEL[0][targetKomaIdx[0]], REEL[1][targetKomaIdx[1]], REEL[2][targetKomaIdx[2]] );
				//console.log("  ", targetKomaIdx[0], targetKomaIdx[1], targetKomaIdx[2] );
				hitCount++;
			}
		}
		return hitCount;
	}

	// SPINボタン処理
	// _thisを使う
	p.spinReel = function () {
		//console.log("レバー引いた");
		_this.mc_lever.play();
		_this.mc_betlamp.gotoAndStop("fr_Off");
		// 抽選
		//_this.fr_lot();
		
		// ライン表示消す
		_this.mc_lineDisplay.setLineVisible(false);

		imgNumCredit.setNumber(sf.creditMedal);

		//ハズレ出目の用意
		if (sf.hitYaku == HAZURE) {
			sf.hitLine = 1; // ハズレのときはライン1で考える
			this.setReelComeOff();
		}
		// メダル消費
		//sf.userMedal -= betLine * betMedal;
	}

	// ワンキーSTOPボタン処理
	// _thisを使う
	// 個別STOPの場合、data.reelIdxを指定して渡す
	p.stopReel = function(data) {
		if (btnPressedCount >= 3) return;
	}
	var reelTweenInfo = [ {}, {}, {} ];
	function startReelStopTween(reelIdx) {
		// 超絶連打に備えて一旦正規化しておく
		komaIdxArray[reelIdx] = normalizeKomaIdx(komaIdxArray[reelIdx]);

		var info = reelTweenInfo[reelIdx];
		info.reelIdx = reelIdx;
		info.komaIdx = komaIdxArray[reelIdx];

		var initialSlowdownSpeed = reelSpeed;
		// TODO: STOPボタン押したときいきなりスピード変えたいならここで変える
		//initialSlowdownSpeed = -reelSpeed;
		// TODO: その他方針が決まったら作り込む

		// 止めるべきkomaIdxを特定する
		var komaIdxTo = searchStopKomaIdx(reelIdx, (initialSlowdownSpeed > 0));

		// reelStopDurationを算出する材料等に使える、目的コマまでの距離
		var komaDistance = Math.abs(komaIdxTo - info.komaIdx);
		//console.log("komaDistance:", komaDistance);
		//reelStopDuration = komaDistance * 80; // elasticOutでこれやると逆に不自然だったりするサンプル

		// そこに向かってtween開始
		cjs.Tween.get(info, {onChange: onChangeReelStopTween})
			.to({komaIdx: komaIdxTo}, reelStopDuration, reelStopEase)
			.call(function(){ reelStoppedArray[reelIdx] = true; });
	}
	function searchStopKomaIdx(reelIdx, direction) {
		var add = direction ? -1 : +1; // 順回転なら減算、逆回転なら加算方向へ走査していく
		var targetKomaIdx;
		if (add > 0) {
			targetKomaIdx = Math.floor(komaIdxArray[reelIdx] + add);
		} else {
			targetKomaIdx = Math.ceil(komaIdxArray[reelIdx] + add);
		}
		//console.log("targetKomaIdx start:", targetKomaIdx);

		if (FREEZE_DEBUG) var cnt = 0;
		while (true) {
			var zgrNow = _this.getHitLineZugara(reelIdx, targetKomaIdx);
			//console.log("  targetKomaIdx, zgrNow:", targetKomaIdx, ",", zgrNow);

			if( (sf.hitYaku == HAZURE) && (normalizeKomaIdx(targetKomaIdx) == hazureKomaIdxArray[reelIdx]) ) { // ランダムハズレパターンの場合
			//if( (sf.hitYaku == HAZURE) && (zgrNow == hazureZugaraArray[reelIdx]) ) { // 固定ハズレパターンの場合
				break;
			} else if(zgrNow == sf.hitYaku) {
				break;
			}

			targetKomaIdx += add;
			/*
			if (FREEZE_DEBUG) {
				cnt++;
				if (cnt > 100) {
					//console.log("無限ループかな。cnt:", cnt);
				}
				if (cnt > 200) {
					//console.log("止めます。");
					break;
				}
			}*/
		}

		//console.log("targetKomaIdx finally:", targetKomaIdx);
		return targetKomaIdx;
	}
	function onChangeReelStopTween(evt) {
		//console.log(evt);
		var tween = evt.currentTarget;
		var reelIdx = tween.target.reelIdx;
		var komaIdx = tween.target.komaIdx;
		komaIdxArray[reelIdx] = komaIdx;
		//console.log(reelIdx, komaIdx);
		_this.fr_reelSet(reelIdx, komaIdx);
	}

	// ベット変化時のコールバック
	// mc_betSelectorから呼ばれる
	// _thisを使う
	p.onBetChange = function(data){
		//console.log("onBetChange data:", data);
		betMedal = data.bet;

		if (DEBUG) { // ベット額によってリール停止パターン変えるテスト
			var DEBUG_REEL_STOP_INFO = {
				1:  {d: 1000, e: cjs.Ease.linear},
				5:  {d: 1000, e: cjs.Ease.elasticOut},
				10: {d: 2500, e: cjs.Ease.sineOut},
				50: {d: 2000, e: cjs.Ease.backInOut},
			}
			var info = DEBUG_REEL_STOP_INFO[betMedal];
			reelStopDuration = info.d;
			reelStopEase = info.e;
			//console.log(reelStopEase);
		}
		_this.checkButtons();
		_this.updateBetNum();

	}

	// ライン変化時の処理
	// _thisを使う
	p.onLineChange = function(data){
		betLine += data.line;

		// ライン増減処理が意図せず重複して走ったりした場合の対策で、
		// 上限下限で抑えておく。
		if (betLine > MAX_LINE) betLine = MAX_LINE;
		if (betLine < 0) betLine = 0;

		//console.log("onLineChange line: ", betLine);

		if (DEBUG) { // ライン数によってリール速度変えるテスト
			var DEBUG_SPEED = [-0.5, -0.2, 0.2, 0.5, 0.7];
			if (betLine > 0) reelSpeed = DEBUG_SPEED[betLine - 1];
		}

		_this.checkButtons();
		_this.updateBetNum();
		_this.mc_lineDisplay.betUpTo(betLine);
	}

	// ボタン押せるかどうかチェック
	p.checkButtons = function(){
		//console.log({sf.userMedal, betMedal, betLine});

		if (sf.userMedal< betMedal * betLine) {
			//console.log("メダル不足のためベットリセット");
			betMedal = 0;
			betLine = 0;
			//this.mc_betSelector.resetBet();
			this.mc_lineDisplay.betUpTo(0);
		}

		// SPINできるかチェック
		this.btn_spin.setButtonEnabled(betMedal > 0 && betLine > 0);

		if(betMedal > 0 && betLine > 0){
			this.mc_message_2.visible = false;
		}else{
			this.mc_message_2.visible = true;
		}
	}

	// ベット数字更新
	p.updateBetNum = function(){
		imgNumBet.setNumber(betMedal * betLine);
	}
	
	//　残りゲーム数のチェック
	p.checkGameLeft = function(){
		//前役がリプレイの場合は残りゲーム数を減らさない
		if(sf.hitYaku == 6) sf.gameLeft++;
		if(sf.gameLeft > 0){
			this.initGame();
		}else{
			this.endGame();
		}

	}

	// レバーが下がりきったときの処理
	// mc_leverから呼ばれる
	p.onLeverDown = function(){
		//レバーが下まで来た！
		//ボタン全点灯
		for(var i = 0; i < 3; i++) {
			this.setEachStopBtnEnabled(i, true);
		}
		imgNumLeft.setNumber(sf.gameLeft);
		this.btn_spin.setButtonEnabled(false);
		this.btn_stop.visible = true;
		this.btn_stop.setButtonEnabled(true);

		cjs.Ticker.on("tick", tickHandler);
		function tickHandler(event){
			if (event.paused) return;

			//リール回転
			if(reelStoppedArray.every( function(elm, idx, arr){return elm;} )){
				//console.log("全部止まった");
				_this.btn_stop.setButtonEnabled(false);
				event.remove();
				_this.reelStopWait();
			}

			for(var cnt = 0; cnt < 3; cnt++){
				if(btnPressedArray[cnt] == false){
					// まだSTOPボタン押してない時に普通の回転処理
					// STOPボタン押したあとはtweenに任せて止まる
					_this.fr_reelRotation(cnt);
				}
			}
		}
		this.drawLeverAction();
	}

	// ライン1から見た各ラインのコマオフセット
	var HITLINE_OFFSET = [
		[ 0, 0, 0], // ライン1
		[-1,-1,-1], // ライン2
		[+1,+1,+1], // ライン3
		[-1, 0,+1], // ライン4
		[+1, 0,-1], // ライン5
	];
	// ライン1が指定komaIdxのときの、当選ライン上の図柄IDを取得
	// このline1KomaIdxは整数のみ許容
	// トゥイーン等で 20→23 のように、MAX_KOMAを超えて調査したいこともあるので
	// line1KomaIdxは正規化されてなくてよい。
	p.getHitLineZugara = function(reelIdx, line1KomaIdx){
		var hitLineKomaIdx = line1KomaIdx + HITLINE_OFFSET[sf.hitLine-1][reelIdx];

		hitLineKomaIdx = normalizeKomaIdx(hitLineKomaIdx);

		return REEL[reelIdx][hitLineKomaIdx];
	}

	// komaIdxを0以上MAX_KOMA未満に正規化する
	function normalizeKomaIdx(komaIdx) {
		while(komaIdx >= MAX_KOMA)
			komaIdx -= MAX_KOMA;
		while(komaIdx < 0)
			komaIdx += MAX_KOMA;
		return komaIdx;
	}

	// 全リール止まったときの処理
	p.reelStopWait = function(){
		// 獲得役
		var getYaku = (sf.hitLine <= betLine) ? sf.hitYaku : HAZURE;

		// 獲得メダル
		//getMedal = betMedal * MAGNIFICATION[getYaku];
		getMedal = sf.addMedal;;
		//user_medal += getMedal;
		var creditMedalOrigin = sf.creditMedal;
		sf.creditMedal += getMedal;
		// エフェクト
		//console.log(getYaku);
		if(getYaku != HAZURE) {
			this.fr_setEffect();
		}

		// 表示用数字をgetMedalまでカウントアップする
		var getMedalDisp = 0;
		var creditMedalDisp = creditMedalOrigin;
		var view_start_time = cjs.Ticker.getTime(true);

		cjs.Ticker.on("tick", tickHandler);
		function tickHandler(event){
			if (event.paused) return;

			var getMedalAdd = Math.max(1, Math.floor((getMedal - getMedalDisp) / 15));
			getMedalDisp += getMedalAdd;
			creditMedalDisp += getMedalAdd;
			if (getMedalDisp > getMedal) getMedalDisp = getMedal;
			if (creditMedalDisp > creditMedalOrigin + getMedal) creditMedalDisp = creditMedalOrigin + getMedal;
	
			var nowTime = cjs.Ticker.getTime(true);
			if((nowTime - view_start_time) >= 1000){
				getMedalDisp = getMedal;
				creditMedalDisp = creditMedalOrigin + getMedal;
				//console.log("getMedal:" + getMedal)

				//console.log("creditMedal:" + sf.creditMedal)
				event.remove();
				//console.log("getYaku" + getYaku);
				if (getYaku == HAZURE || getYaku == ZGR_REPLAY) {
					_this.checkGameLeft();
				}
			}
			imgNumWin.setNumber(getMedalDisp);
			imgNumCredit.setNumber(creditMedalDisp);
		}
	}

	// 指定リールのライン1の図柄が指定コマになるように上中下+下の下コマをセットする
	// komaIdxは小数を許容する
	var ZGR_BASE_Y = 278;
	var ZGR_HEIGHT = 96;
	p.fr_reelSet = function(reelIdx, komaIdx) {
		//if (reelIdx == 0) console.log("fr_reelSet(", reelIdx, ",", komaIdx, ")");

		var upperKomaIdx = normalizeKomaIdx(komaIdx - 1);
		var integerPart = Math.floor(upperKomaIdx);
		var fractionalPart = upperKomaIdx - integerPart;
		for(var i = 0; i < 4; i++){
			var intKoma = normalizeKomaIdx(integerPart + i);

			var zgrIdx = REEL[reelIdx][intKoma];

			var mc_zgr = this["mc_reel" + reelIdx + "_" + i];
			mc_zgr.gotoAndStop(zgrIdx);
			mc_zgr.y = ZGR_BASE_Y + ZGR_HEIGHT * i - (ZGR_HEIGHT * fractionalPart);

			//if (reelIdx == 0) console.log("  mc_reel" + reelIdx + "_" + i, ": zgrIdx[", zgrIdx, "], y[", mc_zgr.y, "]");
			//if (reelIdx == 0) console.log("    intKoma:", intKoma, ", fractionalPart:", fractionalPart);
		}
	}

	// 個別STOPボタン点灯、消灯
	p.setEachStopBtnEnabled = function(idx, enabled) {
		this["mc_btn" + idx].setButtonEnabled(enabled);
	}

	// 任意のリールを1フレームで動かすスピードぶん回転させる
	// komaIdxは小数を許容。fr_reelSetにそのまま渡す。
	p.fr_reelRotation = function(reelIdx) { // frame_55
		komaIdxArray[reelIdx] -= reelSpeed;

		// スピードが大きくてもいいように範囲内でループさせる
		komaIdxArray[reelIdx] = normalizeKomaIdx(komaIdxArray[reelIdx]);
		
		//見た目変更
		var komaIdx = komaIdxArray[reelIdx];
		this.fr_reelSet(reelIdx, komaIdx);
	}

	// 抽選（通信対応前のもの）
	p.fr_lot = function() { // frame_60
		// 仮に当確率で抽選しとく
		hitYaku = Math.floor(Math.random() * YAKU_MAX); // 0〜YAKU_MAX-1
		hitLine = Math.floor(Math.random() * MAX_LINE) + 1 // 1〜MAX_LINE

		if (hitYaku == HAZURE) {
			hitLine = 1; // ハズレのときはライン1で考える
			this.setReelComeOff();
		}
	}

	// 当たった役に応じて演出をセットする
	p.fr_setEffect = function() {
		// ライン点滅
		this.mc_lineDisplay.blinkNum(sf.hitLine);

		// 役に応じたエフェクト
		if(sf.hitYaku != HAZURE && sf.hitYaku != ZGR_REPLAY) {
			// BIG WIN
			this.showBigWin();
		}
	}

	// BIG WIN演出
	p.showBigWin = function(){
		var counter = 0;
		var tickListener = cjs.Ticker.on("tick", tickHandler);

		if(sf.hitYaku <= ZGR_BAR2) this.bonusLampAction(PK_PEKA);

		function tickHandler(event){
			if (event.paused) return;
	
			if(counter == 2){
				medal_get_text.show(getMedal);
				for(var i=0; i<3; i++){
					_this["mc_reel_magic_" + i].visible = false;
				}

			}else if(counter == 3){
				if( sf.hitYaku <= ZGR_SEVEN ) {
					_this.mc_confetti.visible = true;
					//_this.mc_effectmask.visible = true;
				}else if(sf.hitYaku <= ZGR_BAR ){

				}
			}

			if(counter == 20){
				cf.stage.on("click",hideBigWin, null, true, {});
			}

			if(counter == 60){
				hideBigWin()
			}else{
				counter++;
			}
		}

		function hideBigWin(){
			cjs.Ticker.off("tick", tickListener);
			cf.stage.removeAllEventListeners("click");
			medal_get_text.visible = false;	
			//_this.mc_effectmask.visible = false;
			_this.mc_confetti.visible = false;
			_this.checkGameLeft();
		}
	}

	p.setReelComeOff = function() {
		/* 固定ハズレパターンの場合
		var rand = Math.floor(Math.random() * HAZURE_PATTERN.length);
		console.log("ハズレの種類 : " + rand);

		hazureZugaraArray = HAZURE_PATTERN[rand];
		*/

		// ランダムハズレパターンを探索する

		// 初期ランダム候補を決める
		hazureKomaIdxArray = [
			Math.floor(Math.random() * MAX_KOMA),
			Math.floor(Math.random() * MAX_KOMA),
			Math.floor(Math.random() * MAX_KOMA)
		];

		var hitCount = 0;

		// 念のため各リールループでずらして探す想定をするが、
		// だいたいすぐ見つかると思う
		for(var ki0 = 0; ki0 < MAX_KOMA; ki0++){ // 左ループ
			for(var ki1 = 0; ki1 < MAX_KOMA; ki1++){ // 中ループ
				for(var ki2 = 0; ki2 < MAX_KOMA; ki2++){ // 右ループ
					// 成立ライン数を数える
					hitCount = countHitLine(hazureKomaIdxArray);

					if (hitCount == 0) {
						//console.log("ハズレ出目確定！ hazureKomaIdxArray:", hazureKomaIdxArray);
						break;
					}

					//console.log("ハズレ出目探索中... hitCount:", hitCount, "のためNG:", hazureKomaIdxArray);
					hazureKomaIdxArray[2] = normalizeKomaIdx(hazureKomaIdxArray[2] + 1); // 次の出目を調べる準備
				}
				if (hitCount == 0) break;
				hazureKomaIdxArray[1] = normalizeKomaIdx(hazureKomaIdxArray[1] + 1); // 次の出目を調べる準備
			}
			if (hitCount == 0) break;
			hazureKomaIdxArray[0] = normalizeKomaIdx(hazureKomaIdxArray[0] + 1); // 次の出目を調べる準備
		}
	}


//-----------------------------------------------
//　メッセージを表示する
//-----------------------------------------------
p.showMessage = function(txt, y){
	this.mc_message_2.visible = true;
	this.mc_message_2.y =  y;
	this.mc_message_2.x =  lib.properties.width/2;
	this.mc_message_2.mc_message.txt_message.text = txt;
	this.mc_message_2.mc_message.txt_message.y = (this.mc_message_2.getBounds().height - this.mc_message_2.mc_message.txt_message.getBounds().height)/2;
	this.mc_message_2.mc_message.txt_message.x = (this.mc_message_2.getBounds().width - this.mc_message_2.mc_message.txt_message.getBounds().width)/2;
	this.mc_message_2.gotoAndPlay(1);

}

//-----------------------------------------------
//　以下演出
//-----------------------------------------------
//レバーON時演出テーブル
//0:演出なし
//1:チカッ
//2:点灯
var PK_DEFAULT 		= 0;
var PK_SPARKLE 		= 1;
var PK_PEKA    		= 2;

var LEVER_ACTION = [];
LEVER_ACTION[ZGR_SEVEN]  	= [65,35,5];
LEVER_ACTION[ZGR_BAR] 		= [65,35,5];
LEVER_ACTION[ZGR_BAR2] 　	= [65,35,5];
LEVER_ACTION[ZGR_SUIKA]  	= [90,10,0];
LEVER_ACTION[ZGR_BELL] 		= [90,10,0];
LEVER_ACTION[ZGR_CHERRY]  	= [90,10,0];
LEVER_ACTION[ZGR_REPLAY] 	= [90,10,0];
LEVER_ACTION[HAZURE]  		= [100,0,0];

//ストップボタン押下時演出テーブル
//0:演出なし
//1:1リール雷
//2:2リール雷
//3:3リール雷

var TH_NONE 		= 0;
var TH_REEL1 		= 1;
var TH_REEL2   		= 2;
var TH_REEL3   		= 3;

var STOPON_ACTION = [];
STOPON_ACTION[ZGR_SEVEN] 	= [10,0,0,90];
STOPON_ACTION[ZGR_BAR] 		= [10,0,0,90];
STOPON_ACTION[ZGR_BAR2] 	= [10,0,0,90];
STOPON_ACTION[ZGR_SUIKA] 	= [75,0,25,0];
STOPON_ACTION[ZGR_BELL] 	= [75,0,25,0];
STOPON_ACTION[ZGR_CHERRY] 	= [75,25,0,0];
STOPON_ACTION[ZGR_REPLAY] 	= [75,25,0,0];
STOPON_ACTION[HAZURE] 		= [75,25,0,0];


//リールストップ演出フラグ
var reelStopAction = 0;
var stopCancelFlag = false;

//-----------------------------------------------
//　レバーON時の抽選
// 0　ナシ
// 1 チカっとする
// 2 ぺかる
//-----------------------------------------------
p.drawLeverAction = function(){
	if(pekaFlag) return;

	//チャンスランプ演出
	var lot = this.lottery(LEVER_ACTION);
	if(lot == 1) this.bonusLampAction(PK_SPARKLE);
	if(lot == 2){
		this.bonusLampAction(PK_PEKA);
		pekaFlag = true;
	}

	//リールストップ演出
	reelStopAction　= this.lottery(STOPON_ACTION);
	if(reelStopAction　!== TH_NONE){
		_this.mc_effectmask.alpha = 0;
	 	_this.mc_effectmask.visible = true;
	 	_this.mc_chara.visible = true;
	 	_this.mc_chara.x = 1000;
		createjs.Tween.get(_this.mc_effectmask, {loop:false})
			.to({alpha:0.6},500);
		createjs.Tween.get(_this.mc_chara, {loop:false})
			.to({x:650}, 500, createjs.Ease.backOut);

	}
}

//-----------------------------------------------
// ストップボタンON押下時に抽選
//0:演出なし
//1:1リール雷
//2:2リール雷
//3:3リール雷
//-----------------------------------------------
p.pushStopButton = function(data) {
	//console.log(data);
	//console.log(btnPressedCount);
	//console.log(reelStopAction　);
	if(stopCancelFlag) return;

	stopIdx = -1;
	if (data && data.hasOwnProperty('reelIdx')) {
		// どのリールか指定あり
		if (!btnPressedArray[data.reelIdx]) {
			stopIdx = data.reelIdx;
		}
	} else {
		// まだ押してないリールを探す
		stopIdx = btnPressedArray.indexOf(false);
	}
	_this.mc_effectmask.visible = false;

	//console.log(stopIdx);
	if (stopIdx >= 0) {
		btnPressedCount++;
		btnPressedArray[stopIdx] = true;
		_this.setEachStopBtnEnabled(stopIdx, false);
		// 2016-12-02 リール停止トゥイーンを開始
		startReelStopTween(stopIdx);

		switch(btnPressedCount){
			case 3:
				if(reelStopAction == TH_REEL3){
					sucessThunder(stopIdx);
				}else if(reelStopAction == TH_REEL2){
					missThunder(stopIdx);
				}
				break;
			case 2:
				if(reelStopAction >= TH_REEL2 ){
					sucessThunder(stopIdx);
				}else if(reelStopAction == TH_REEL1){
					missThunder(stopIdx);
				}
				break;
			case 1:
				if(reelStopAction >= TH_REEL1 ){
					sucessThunder(stopIdx);
				}
				break;
			default:
				break;
		}
	}
	//雷成功
	function sucessThunder(reel_num){
		_this.mc_panelmask.visible = true;
		_this.mc_chara.visible = true; 
		stopCancelFlag = true;
		for(var i=0; i<3; i++){
			_this["mc_reelmask_" + i].visible = !btnPressedArray[i];
		}
		var thunderObj = _this["mc_thunder_" + stopIdx];
		var reelMaskObj =  _this["mc_reelmask_" + stopIdx];
		var magicObj =  _this["mc_reel_magic_" + stopIdx];

		thunderObj.visible = true;
		createjs.Tween.get(thunderObj, {loop:false})
			.call(function(){
				_this.mc_chara.gotoAndPlay("fr_shot");
				})
			.wait(50)
			.call(function(){
				thunderObj.gotoAndPlay(0);
				})
			.wait(150)
			.call(function(){
				reelMaskObj.visible = false;
				magicObj.visible = true;
				})
			.wait(200)
			.call(function(){
				stopCancelFlag = false;
				_this.mc_chara.gotoAndPlay("fr_standby");
				if(btnPressedCount == 3){
					_this.mc_chara.gotoAndPlay("fr_presure");
					_this.mc_panelmask.visible = false;
	
				}
			})
	}

	//雷失敗
	function missThunder(reel_num){
		_this.mc_panelmask.visible = true;
		_this.mc_chara.visible = true; 
		stopCancelFlag = true;
		var thunderObj = _this["mc_thunder_" + stopIdx];
		for(var i=0; i<3; i++){
			_this["mc_reelmask_" + i].visible = !btnPressedArray[i];
		}

		var thunderObj = _this["mc_thunder_" + stopIdx];
		var reelMaskObj =  _this["mc_reelmask_" + stopIdx];
		thunderObj.visible = true;
		createjs.Tween.get(thunderObj, {loop:false})
			.call(function(){
				_this.mc_chara.gotoAndPlay("fr_shot");
				})
			.wait(50)
			.call(function(){
				thunderObj.gotoAndPlay(0);
				})
			.wait(150)
			.call(function(){
				reelMaskObj.visible = false;
				//magicObj.visible = true;
				})
			.wait(300)
			.call(function(){
				_this.mc_chara.gotoAndPlay("fr_dissappointed");
				createjs.Tween.get(_this.mc_chara)
					.to({x:1000}, 1000)
				})
			.wait(150)
			.call(function(){
					for(var i=0; i<3; i++){
						_this["mc_reelmask_" + i].visible = false;
					}
				})
			.wait(200)
			.call(function(){
				_this.mc_panelmask.visible = false;
				stopCancelFlag = false;
				_this.mc_chara.gotoAndPlay("fr_default");
				if(btnPressedCount == 3){
					_this.mc_panelmask.visible = false;
					
				}
			})
	}

}

p.releaseStopButton = function() {

}
//-----------------------------------------------
// ストップボタンOFF押下時に抽選
// 0　ナシ
// 1 ペカる
//-----------------------------------------------
p.drawStopOffAction = function(){
	var lot = this.lottery(STOPON_ACTION);
}

//-----------------------------------------------
//　抽選する
//-----------------------------------------------
p.lottery = function(table){
	//console.log("betLine:" + betLine );
	//console.log("sf.hitLine:" + sf.hitLine );
	if(betLine < sf.hitLine ) return;
	var sum  =0;
	var temp =0;
	//分母
	for(var i=0; i<table[sf.hitYaku].length; i++){
		sum += table[sf.hitYaku][i];
	}
	//console.log("sum:" + sum);
	//抽選
	var rand =  Math.floor(Math.random()*sum + 1);
	var randx = rand + btnPressedCount * 10;
	if( randx >= sum) randx = sum;

	for(var i=0; i<table[sf.hitYaku].length; i++){
		temp += table[sf.hitYaku][i];
		if(randx <= temp){
			//console.log("temp:" + i);
			return i;
		}
	}
}

//-----------------------------------------------
//　チャンスランプアクション
//-----------------------------------------------
p.bonusLampAction = function(action){
	//console.log("action:" + action);
	switch(action){
		case PK_DEFAULT :
			this.mc_chancelight.visible = false;
			break;
		case PK_SPARKLE :
			this.mc_chancelight.visible = true;
			sparkleObj(this.mc_chancelight);
			break;
		case PK_PEKA :
			pekaFlag = true;
			this.mc_chancelight.alpha = 0;
			this.mc_chancelight.visible = true;
			createjs.Tween.get(this.mc_chancelight, {loop:false})
			.to({alpha:1}, 100);
			break;
		default :
			break;
	}

	//点滅
	function  sparkleObj(obj){
		createjs.Tween.get(obj, {loop:false})
			.to({alpha:0.8}, 200)
			.to({alpha:0}, 200);
	}

}




})(lib = lib||{}, createjs = createjs||{});

