(function (lib, cjs) {

	var p = lib.Mc_lineDisplay.prototype;
	var _this = null; // インスタンスを実行時に保持

	var MAX_LINE = 5;

	var betStateDict = {};
	var lineVisible = true;

	var blinkListener = null;

	// 一度だけの初期化
	p.initOnce = function() {
		_this = this;

		for (var l=1; l<=MAX_LINE; l++) {
			betStateDict[l] = false;
			this["line_" + l].visible = false;
		}
	}
	// ベット状態、ライン表示するかどうかによって見た目更新する
	p.updateDisplay = function(){
		for (var l=1; l<=MAX_LINE; l++) {
			var mc_num = this["num_" + l];
			if (betStateDict[l]) {
				mc_num.gotoAndStop("on");
			} else {
				mc_num.gotoAndStop("off");
			}
			this["line_" + l].visible = betStateDict[l] && lineVisible;
		}
	}
	// ラインnumまでベット状態として光らせる
	// 例）
	// num == 3 なら1と2と3
	// num == 0 ならなし
	p.betUpTo = function(num){
		for (var l=1; l<=MAX_LINE; l++) {
			betStateDict[l] = (l <= num);
		}
		this.updateDisplay();
	}
	// ラインの表示非表示切り替え
	p.setLineVisible = function(visible){
		lineVisible = visible;
		this.updateDisplay();
	}
	// ラインnumを点滅させる
	p.blinkNum = function(num){
		blinkListener = cjs.Ticker.on("tick", tickHandler);
		var counter = 0;
		function tickHandler(event){
			if (event.paused) return;

			counter++;
			var mc_num = _this["num_" + num];
			var CYCLE_FRAME = 6;
			if (counter % CYCLE_FRAME < CYCLE_FRAME / 2) {
			//if (event.time % CYCLE_MS < CYCLE_MS / 2) {
				mc_num.gotoAndStop("on");
				_this["line_" + num].visible = true;
			} else {
				mc_num.gotoAndStop("off");
				_this["line_" + num].visible = false;
			}
		}
	}
	// ラインの点滅を止める
	p.stopBlink = function(){
		if (blinkListener) {
			cjs.Ticker.off("tick", blinkListener);
			blinkListener = null;
		}
		this.updateDisplay();
	}

})(lib = lib||{}, createjs = createjs||{});

