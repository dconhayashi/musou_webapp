(function (lib, cjs) {

	var p = lib.Mc_betSelector.prototype;
	var _this = null; // インスタンスを実行時に保持

	var betChangeCallback = null;

	var bet_medal = 0;

	// 一度だけの初期化
	p.initOnce = function() {
		_this = this;

		this.btn_bet_01.addButtonFunc(this.setBet, {bet:1});
		this.btn_bet_05.addButtonFunc(this.setBet, {bet:5});
		this.btn_bet_10.addButtonFunc(this.setBet, {bet:10});
		this.btn_bet_50.addButtonFunc(this.setBet, {bet:50});

		this.mc_betselected_01.visible = false;
		this.mc_betselected_05.visible = false;
		this.mc_betselected_10.visible = false;
		this.mc_betselected_50.visible = false;
	}
	p.setCallback = function(cb){
		betChangeCallback = cb;
	}
	p.resetBet = function(){
		this.setBet({bet:0});
	}
	p.setBet = function(data){
		betChangeCallback(data);
		bet_medal = data.bet;

		_this.btn_bet_01.visible = true;
		_this.btn_bet_05.visible = true;
		_this.btn_bet_10.visible = true;
		_this.btn_bet_50.visible = true;
		_this.mc_betselected_01.visible = false;
		_this.mc_betselected_05.visible = false;
		_this.mc_betselected_10.visible = false;
		_this.mc_betselected_50.visible = false;

		switch(data.bet){
			case 1:
				_this.btn_bet_01.visible = false;
				_this.mc_betselected_01.visible = true;
				break;
			case 5:
				_this.btn_bet_05.visible = false;
				_this.mc_betselected_05.visible = true;
				break;
			case 10:
				_this.btn_bet_10.visible = false;
				_this.mc_betselected_10.visible = true;
				break;
			case 50:
				_this.btn_bet_50.visible = false;
				_this.mc_betselected_50.visible = true;
				break;
		}
	}
	p.checkButtons = function(bet_line, user_medal){
		this.btn_bet_01.setButtonEnabled(bet_line * 1 <= user_medal);
		this.btn_bet_05.setButtonEnabled(bet_line * 5 <= user_medal);
		this.btn_bet_10.setButtonEnabled(bet_line * 10 <= user_medal);
		this.btn_bet_50.setButtonEnabled(bet_line * 50 <= user_medal);
	}
	p.setButtonsEnabled = function(enabled){
		this.btn_bet_01.setButtonEnabled(enabled);
		this.btn_bet_05.setButtonEnabled(enabled);
		this.btn_bet_10.setButtonEnabled(enabled);
		this.btn_bet_50.setButtonEnabled(enabled);
	}

})(lib = lib||{}, createjs = createjs||{});

