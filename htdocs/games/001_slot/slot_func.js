var sf = sf||{};
var cjs = createjs;
var DEBUG_MODE = false;
if(!location.hostname){
	DEBUG_MODE = true;
}
(function(){

//------------------------------------------------
//　グローバル変数
//------------------------------------------------
//ユーザのメダル
sf.userMedal = 0;
sf.addMedal  = 0;
sf.hitYaku   = null;
sf.hitLine   = null;
sf.betMedal  = 0;
sf.betLine   = 0;
sf.gameLeft  = 0;
sf.gameCount = 0;
sf.playCount = 0;
sf.retryLeft = 0;
//sf.sendData  = '';
var prizeArr = [];



//------------------------------------------------
//　API(init)
//------------------------------------------------
sf.apiInit = function(){
	sf.apiInitSuccess()
}

sf.apiInitSuccess = function(data){
	exportRoot.gotoAndStop("fr_Game");
} 

sf.apiInitError = function(data){
	cf.jump(cf.back_url, {}, null, null );
} 

sf.apiBet = function(){
	_log("vars" ,vars);
	cf.postMessage("playStart");
	cf.stage.removeAllEventListeners();
	sf.gameCount = 0;
	if(DEBUG_MODE){
		var data ={};
		data.data ={};
		data.data.default_medals = 50;
		data.data.game_left = 1;
		data.data.retry_left = 3;
		data.data.limit_retry = 3;
		data.data.regist_url = cf.back_url;
		data.data.pleasant_medals = 100;
		data.data.result_array = [
			{yaku:5, line:2, medal:100},	//はずれ
			{yaku:0, line:2, medal:200},　
			{yaku:2, line:1, medal:300},
			{yaku:3, line:1, medal:300},
			{yaku:4, line:1, medal:300},
			{yaku:5, line:1, medal:300},
			{yaku:6, line:1, medal:300},
			{yaku:7, line:1, medal:300},
			{yaku:1, line:1, medal:300},
			{yaku:1, line:1, medal:300},
			{yaku:1, line:1, medal:300},
		];
		sf.apiBetSuccess(data);
	}else{
		cf.api("slot/start2/001_slot", vars, sf.apiBetSuccess, sf.apiBetError );
	}
}


sf.apiBetSuccess = function(data){
	_log("apiBetSuccess", data);
	sf.playCount++;
	sf.userMedal = Number(data.data.default_medals);
	sf.creditMedal 	= Number(data.data.default_medals);
	sf.gameLeft  	= Number(data.data.game_left);
	sf.retryLeft    = Number(data.data.limit_retry) - sf.playCount + 1;
	sf.registUrl 	= data.data.regist_url;
    prizeArr 		= data.data.result_array;
    sf.presantMedal = Number(data.data.pleasant_medals);
    sf.sendData 	= data.data.b;
	//exportRoot.gotoAndStop("fr_Game");
	//console.log(prizeArr);
	exportRoot.mc_game.initGame();
} 

sf.apiBetError = function(data){
	cf.jump(cf.back_url, {}, null, null );
} 

sf.bet = function(){
	sf.gameLeft--;
	sf.hitYaku 	= Number(prizeArr[sf.gameCount].yaku);
	sf.hitLine 	= Number(prizeArr[sf.gameCount].line);
	sf.addMedal = Number(prizeArr[sf.gameCount].medal);
	//console.log([sf.hitYaku,sf.hitLine,sf.addMedal]);
	sf.userMedal +=sf.addMedal;
	exportRoot.mc_game.spinReel();
	sf.gameCount++;
}

sf.apiResult = function(){
	
}


sf.regist = function(){
	_log("sendData:" + sf.sendData)
	sampleForm(sf.sendData);
	function sampleForm( value ){
		var form = document.createElement( 'form' );
	    document.body.appendChild( form );
	    var input = document.createElement( 'input' );
	    input.setAttribute( 'type' , 'hidden' );
	    input.setAttribute( 'name' , 'b' );
	    input.setAttribute( 'value' , value );
	    form.appendChild( input );
	    form.setAttribute( 'action' , sf.registUrl );
	    form.setAttribute( 'method' , 'post' );
	   	form.setAttribute( 'target' , '_parent' );
	    form.submit();
	}
}
//------------------------------------------------

//ムービークリップをボタン化する。
cjs.MovieClip.prototype.addButtonFunc = function(func, data, func2){
	this.bounds = this.getBounds();
	var _this = this;

	//リスナー用のシェイプとそのヒットエリアを作る
	if(this.bounds){ //Bitmapかスプライトの場合
		this.hitShape = new createjs.Shape();
		var hitAreaShape = new createjs.Shape();
		hitAreaShape.graphics.beginFill("rgba(255,0,0,1)");
		hitAreaShape.graphics.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
		this.hitShape.hitArea = hitAreaShape;
		this.addChild(this.hitShape);
		this.hitShape.on("mousedown", mousedownHandler, null, false, data);
		this.hitShape.on("pressup", pressupHandler, null, false, data);
	}else{　//シェイプの場合
		this.on("mousedown", mousedownHandler, null, false, data);
		this.on("pressup", pressupHandler, null, false, data);
	}
	
	function mousedownHandler(event){
		_this.scaleX = 0.9;
		_this.scaleY = 0.9;
		if(func2){
			func2(data);
		}
	}

	var waitTimer = 0;
	function pressupHandler(event){
		_this.scaleX = 1;
		_this.scaleY = 1;
		if(waitTimer == 0){
			func(data);
			wait();
		}
		function wait(){
			createjs.Ticker.addEventListener('tick', tickHandler);
			function tickHandler(event){
				waitTimer++;
				if(waitTimer > 3){
					waitTimer = 0;
					createjs.Ticker.removeEventListener('tick', tickHandler);
				}
			}
		}
	}
}

//ボタンのアクティブ化
cjs.MovieClip.prototype.setButtonEnabled = function(flag){
	// 各状態専用画像がある場合はこれらのラベル名をつけておく
	var ENABLED_LABELNAME = "enabled";
	var DISABLED_LABELNAME = "disabled";

	//console.log(this.labels);
	if(flag){
		this.mouseEnabled = true;
		if(this.bounds){
			if (this.timeline.resolve(ENABLED_LABELNAME) != null) {
				this.gotoAndStop(ENABLED_LABELNAME);
			} else {
				this.filters = [];
				this.cache(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
			}
		}
	}else{
		this.mouseEnabled = false;
		if(this.bounds){
			if (this.timeline.resolve(DISABLED_LABELNAME) != null) {
				this.gotoAndStop(DISABLED_LABELNAME);
			} else {
				var matrix =  new createjs.ColorMatrix(-100,0,0,0);
				this.filters = [
					new createjs.ColorMatrixFilter(matrix),
				];
				this.cache(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
			}
		}
	}
}
//-------------------------------------------------------------------------------------
// メダルゲット文字
//-------------------------------------------------------------------------------------

sf.MedalGetText = function(data){
	cjs.Container.call(this);
	this.visible = false;
	//this.max = data.max ? data.max : 99999999;
	this.width = 750;
	this.height = 224;
	this.regX = this.width/2;
	this.regY = this.height/2;
	this.x = lib.properties.width/2;
	this.y = -200;
	this.textMargin = 0;
	var data ={align:"center"};
	this.imageNumber = new cjs.ImageNumber("images/gold_number.png",173,224,data);
	this.imageNumberWidth = 173;

	this.medalGetImage = new cjs.Bitmap("images/get.png");
	this.medalGetImage.y = 160;
	this.medalGetImageWidth = 134;

	this.regBonusImage = new cjs.Bitmap("images/reg_bonus.png");
	this.regBonusImage.y = -110;
	this.regBonusImageWidth = 452;
	this.regBonusImage.x = (this.width - this.regBonusImageWidth)/2

	this.bigBonusImage = new cjs.Bitmap("images/big_bonus.png");
	this.bigBonusImage.y = -110;
	this.bigBonusImageWidth = 457;
	this.bigBonusImage.x = (this.width - this.bigBonusImageWidth)/2
	this.addChild(this.imageNumber);
	this.addChild(this.medalGetImage);


	this.regBonusImage.visible = false;
	this.bigBonusImage.visible = false;
	this.addChild(this.regBonusImage);
	this.addChild(this.bigBonusImage);

	//this.show(500,null,null,0);
}
cjs.extend(sf.MedalGetText, cjs.Container);
var p = sf.MedalGetText.prototype;
p.show = function(number, waitTime){
	if(!waitTime) waitTime = 0;
 	this.visible = true;
	this.alpha = 1;
	this.scaleX = 1;
	this.scaleY = 1;
	this.regBonusImage.visible = false;
	this.bigBonusImage.visible = false;

	if(number && this.max){
		if(number > this.max){
			number = this.max;
		}
	}

	if(number >= 200){
		this.bigBonusImage.visible = true;
	}else if(number >= 100){
		this.regBonusImage.visible = true;
	}

	this.imageNumber.setNumber(number);
	var numbersLength =  number.toString().length;
	//var textWidth =  this.imageNumberWidth * numbersLength + this.medalGetImageWidth + this.textMargin;
	var textWidth =  this.imageNumberWidth * numbersLength;
	var leftX = (this.width - textWidth)/2;
	this.imageNumber.x = leftX
	this.medalGetImage.x = leftX +  this.imageNumberWidth　* numbersLength - this.medalGetImageWidth + 30;
	this.y = -200;


	createjs.Tween.get(this)
     .wait(waitTime)
     .to({y:400}, 500, createjs.Ease.backOut)
     .wait(200)
     //.call(callBack, [callBackParam]);
}
//-------------------------------------------------------------------------------------
// メダルトータル文字
//-------------------------------------------------------------------------------------//トータル
sf.MedalTotalText = function(data){
	cjs.Container.call(this);
	this.visible = false;
	//this.max = data.max ? data.max : 99999999;
	this.width = 750;
	this.height = 224;
	this.regX = this.width/2;
	this.regY = this.height/2;
	this.x = lib.properties.width/2;
	this.y = -200;
	this.textMargin = 0;
	var data ={align:"left"};
	this.imageNumber = new cjs.ImageNumber("images/gold_number.png",173,224,data);
	this.imageNumberWidth = 173;

	this.medalGetImage = new cjs.Bitmap("images/get.png");
	this.medalGetImage.x = 550;
	this.medalGetImage.y = 160;


	this.totalImage = new cjs.Bitmap("images/total.png");
	this.totalImage.x = 100;
	this.totalImage.y = -110;


	this.medalGetImageWidth = 134;

	this.addChild(this.imageNumber);
	this.addChild(this.medalGetImage);
		this.addChild(this.totalImage);
}
cjs.extend(sf.MedalTotalText, cjs.Container);
var p = sf.MedalTotalText.prototype;
p.show = function(number, waitTime, callBack){
	if(!waitTime) waitTime = 400;
 	this.visible = true;
 	this.y = -200;
	if(number && this.max){
		if(number > this.max){
			number = this.max;
		}
	}
	this.imageNumber.setNumber(0);
	var numbersLength =  1;
	var textWidth =  this.imageNumberWidth * numbersLength;
	var leftX = (this.width - textWidth)/2;
	this.imageNumber.x = leftX
     // 表示用数字をnumberまでカウントアップする
	var numDisp = 0;

	var view_start_time = cjs.Ticker.getTime(true);
    var _this = this;
    this.callBack = callBack;
    
	createjs.Tween.get(this)
     .wait(waitTime)
     .to({y:400}, 500, createjs.Ease.backOut)
     .wait(200)
     .call(function(){
     	startCounter();
     })
    
    function startCounter(){
		createjs.Ticker._inited = false;
		createjs.Ticker.init();
		createjs.Ticker.addEventListener('tick',createjs.Tween);
     	cjs.Ticker.on("tick", tickHandler);
		function tickHandler(event){
			if (event.paused) return;
			var numberAdd = Math.max(1, Math.floor((number - numDisp) / 7));
			numDisp += numberAdd;
			if (numDisp > number) numDisp = number;
			var nowTime = cjs.Ticker.getTime(true);
			if((nowTime - view_start_time) >= 1000 || numDisp == number){
				numDisp = number;
				event.remove();
				finish();
			}
			_this.imageNumber.setNumber(numDisp);
			var numbersLength =  numDisp.toString().length;
			var textWidth =  _this.imageNumberWidth * numbersLength;
			var leftX = (_this.width - textWidth)/2;
			_this.imageNumber.x = leftX;
		}
     	
    }
    function finish(){
    	createjs.Tween.get(_this)
    		.wait(300)
    		.to({scaleX:1.6, scaleY:1.6}, 400,  createjs.Ease.BackOut)
    		.to({scaleX:0.9, scaleY:0.9}, 200,  createjs.Ease.BackOut)
    		.to({scaleX:1.1, scaleY:1.1}, 50,  createjs.Ease.BackOut)
    		.call(function(){
    			callBack();
    		});
    }


}

})();
