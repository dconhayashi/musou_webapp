(function (lib, cjs) {

var p = lib.MC_Help.prototype;
var _this = null; // インスタンスを実行時に保持
var now_page = 1;
var now_tab = 1;

p.initHelp = function(){
	_this = this;
	_this.btn_close.on("click", cf.hide_help, null, false, {});
	//ボタン表示
	_this.btn_tab1.on("click", changeTab, null, false, {tab:1});
	_this.btn_tab2.on("click", changeTab, null, false, {tab:2});
	_this.btn_next.on("pressup", nextPage,  null, false, {});
	_this.btn_prev.on("pressup", prevPage,  null, false, {});
	_this.btn_next.on("mousedown", mousedownHandler,  null, false, {});
	_this.btn_prev.on("mousedown", mousedownHandler,  null, false, {});
	_this.mc_mask.addEventListener("click", function(){});


	//ルールのみの場合
	if(_this.pageMax[1] == 0){
		_this.btn_tab2.visible = false;
		_this.btn_tab1.visible = true;
		_this.btn_tab1.x = 375;
		_this.btn_tab1.on("click", changeTab, null, false, {tab:1});
		_this.btn_tab1.gotoAndStop("fr_On");
	}else{
		_this.btn_tab1.visible = true;
		_this.btn_tab2.visible = true;
		//ボタン表示
		_this.btn_tab1.on("click", changeTab, null, false, {tab:1});
		_this.btn_tab2.on("click", changeTab, null, false, {tab:2});
		_this.btn_tab1.gotoAndStop("fr_On");
		_this.btn_tab2.gotoAndStop("fr_Off");
	}
	now_page = 1;
	now_tab = 1;
	_this.showPage(1, 1);
}

//ページ表示
p.showPage = function(tab, page){
	now_page = page;
	now_tab = tab;
	var page_name = "fr_Tab" + now_tab + "_" + now_page;
	if(now_tab == 1){
		_this.btn_tab1.gotoAndStop("fr_On");
		_this.btn_tab2.gotoAndStop("fr_Off");
	}else{
		_this.btn_tab1.gotoAndStop("fr_Off");
		_this.btn_tab2.gotoAndStop("fr_On");	
	}
	
	//1ページしかない時はページ送りのボタンを表示しない
	if(_this.pageMax[now_tab - 1] == 1){
		_this.btn_next.visible = false;
		_this.btn_prev.visible = false;
	}else{
		_this.btn_next.visible = true;
		_this.btn_prev.visible = true;
		
	}
	
	//最大ページ目
	if(_this.pageMax[now_tab -1] == now_page){
		setButtonEnabled(_this.btn_next, false);
		
	}else{
		setButtonEnabled(_this.btn_next, true);
	}
	
	//1ページ目
	if(now_page == 1){
		setButtonEnabled(_this.btn_prev, false);
	}else{
		setButtonEnabled(_this.btn_prev, true);

	}
	
	_this.gotoAndStop(page_name);
}

function changeTab(event,data){
	if(data.tab == now_tab){
		return;
	}
	now_tab = data.tab;
	_this.showPage(now_tab, 1);
}

function nextPage(event){
	event.target.parent.scaleX = 1;
	event.target.parent.scaleY = 1;
	if(now_page >= _this.pageMax[now_tab-1]){
		return;
	}
	now_page++;
	_this.showPage(now_tab, now_page)
}

function prevPage(event){
	event.target.parent.scaleX = 1;
	event.target.parent.scaleY = 1;
	if(now_page == 1){
		return;
	}
	now_page--;
	_this.showPage(now_tab, now_page)
}

function mousedownHandler(event){
	var mc = event.target.parent;
	mc.scaleX = 0.9;
	mc.scaleY = 0.9;	
}

function setButtonEnabled(mc, flag){
	mc.bounds = mc.getBounds();
	if(flag){
		mc.mouseEnabled = true;
		var matrix =  new createjs.ColorMatrix(0,0,0,0);
		mc.filters = [
		    new createjs.ColorMatrixFilter(matrix),
		];
		mc.cache(mc.bounds.x, mc.bounds.y, mc.bounds.width, mc.bounds.height);
	}else{
		mc.mouseEnabled = false;
		var matrix =  new createjs.ColorMatrix(-100,0,0,0);
		mc.filters = [
		    new createjs.ColorMatrixFilter(matrix),
		];
		mc.cache(mc.bounds.x, mc.bounds.y, mc.bounds.width, mc.bounds.height);
	}
}

})(lib = lib||{}, createjs = createjs||{});