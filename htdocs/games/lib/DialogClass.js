(function(){
var cjs = createjs;
var version ="20170222";
var dialogBg = new cjs.Bitmap("../lib/images/dialog.png?"+version);

//==================================================
// ダイアログ
//==================================================
/* sample
pf.doubleup_dialog = new createjs.Dialog();
pf.doubleup_dialog.setPanel("L");
pf.doubleup_dialog.setPosition(200);
pf.doubleup_dialog.setText("ダブルアップに挑戦しますか？");
pf.doubleup_dialog.button_0.setStatus("はい", "btn_a", test_yes, {} );
pf.doubleup_dialog.button_1.setStatus("いいえ", "btn_c", test_yes, {} );
pf.doubleup_dialog.button_0.setButtonEnabled(false);
*/

var ssDialogParam ={
	images: [dialogBg.image.src],
	frames:[
			[660, 258, 208, 62],
			[660, 320, 208, 62],
			[660, 134, 408, 62],
			[660, 196, 408, 62],
			[0, 0, 660, 514],
			[0, 514, 660, 284],
			[0, 798, 660, 204],
			[660, 0, 660, 134]
	],
	animations: {"btn_a":0, "btn_b": 1, "btn_c": 2, "btn_d": 3, "bg_ll": 4, "bg_l": 5, "bg_m": 6, "bg_s":7}

}
var ssDialog = new cjs.SpriteSheet(ssDialogParam);

//==================================================
cjs.Dialog = function(data){
	cjs.Container.call(this);
	this.visible = false;

	this.bg_help = new cjs.Sprite(ssDialog, "bg_help");
	this.bg_ll = new cjs.Sprite(ssDialog, "bg_ll");
	this.bg_l = new cjs.Sprite(ssDialog, "bg_l");
	this.bg_m = new cjs.Sprite(ssDialog, "bg_m");
	this.bg_s = new cjs.Sprite(ssDialog, "bg_s");

	this.backGround = new cjs.Shape();
	this.backGround.graphics.beginFill("rgba(0,0,0,0.8)");
	this.backGround.graphics.drawRect(0,0,lib.properties.width, lib.properties.height);
	this.backGround.addEventListener("click", function(){});

	this.addChild(this.backGround);

	//----------------------------------------------------
	//パネル
	//----------------------------------------------------
	this.panel = new cjs.Container();
	this.panelBackContainer = new cjs.Container();
	this.panelSprite = this.bg_l;
	this.panel_dy = 0;
	
	this.panelBackContainer.addChild(this.panelSprite);
	this.panel.addChild(this.panelBackContainer);
	this.addChild(this.panel);

	//----------------------------------------------------
	// テキスト
	//----------------------------------------------------
	this.dialogText = new cjs.Text("---", "bold 28px 'Arial'", "#FFF");
	this.panel.addChild(this.dialogText);

	this.titleText = new cjs.Text("", "bold 40px 'Arial'", "#FFF");
	this.panel.addChild(this.titleText);
	
	//----------------------------------------------------
	// ボタン
	//----------------------------------------------------
	var _this = this;
	this.button_0 = new cjs.DialogButton("はい","btn_a", function(){_this.hide()}, {});
	this.button_0.visible = false;
	this.panel.addChild(this.button_0);

	this.panel.addChild(this.button_0);
	this.button_1 = new createjs.DialogButton("いいえ", "btn_a",function(){_this.hide()}, {});
	this.button_1.visible = false;
	this.panel.addChild(this.button_1);

	this.buttonOnCount = 0;

	this.button_0.setButtonEnabled(false);
	this.button_1.setButtonEnabled(false);


	//this.setLayout();
}
cjs.extend(cjs.Dialog, cjs.Container);
var p = cjs.Dialog.prototype;
//----------------------------------------------------
//ダイアログをレイアウトし直す
//----------------------------------------------------
p.setPosition = function(dy){
	this.panel_dy = dy;
}

p.setLayout = function(){

	this.panelWidth  = this.panelSprite.getBounds().width;
	this.panelHeight = this.panelSprite.getBounds().height;

	this.panel.regX = this.panelWidth/2;
	this.panel.regY = this.panelHeight/2;

	this.panel.x = lib.properties.width/2;
	this.panel.y = lib.properties.height/2 + this.panel_dy;

	var BUTTON_MARGIN = 6;

	this.dialogText.x = (this.panelWidth - this.dialogText.getBounds().width)/2;
	if(this.titleText.text){	
		this.titleText.x = (this.panelWidth - this.titleText.getBounds().width)/2;
		this.titleText.y = 25;
	}
	if(!this.button_0.visible && !this.button_1.visible){
		this.dialogText.y = (this.panelHeight - this.dialogText.getBounds().height)/2 + this.titleText.y;
	}else if(this.button_0.visible && !this.button_1.visible || !this.button_0.visible && this.button_1.visible){
		this.button_0.x = this.panelWidth/2;
		this.button_0.y = this.panelHeight - 60;
		this.button_1.x = this.panelWidth/2;
		this.button_1.y = this.panelHeight - 60;
		this.dialogText.y = (this.panelHeight - this.dialogText.getBounds().height)/2 - 50 +  this.titleText.y;
	}else if(this.button_0.visible && this.button_1.visible){
		if(this.button_0.width + this.button_1.width > this.panelWidth ){
			//縦に並べる
			this.button_0.x = this.panelWidth/2;
			this.button_0.y = this.panelHeight - 120
			this.button_1.x = this.panelWidth/2;
			this.button_1.y = this.panelHeight - 50;
			this.dialogText.y = (this.panelHeight - this.dialogText.getBounds().height)/2 - 75;
		}else{
			this.button_0.y = this.panelHeight - 60;
			this.button_1.y = this.panelHeight - 60;
			this.button_0.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_0.width/2;
			this.button_1.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_1.width/2 + this.button_0.width + BUTTON_MARGIN;
			this.dialogText.y = (this.panelHeight - this.dialogText.getBounds().height)/2 - 30;
		}
	}
	this.dialogText.x = (this.panelWidth - this.dialogText.getBounds().width)/2;	
}

//----------------------------------------------------
//パネルを変える
//----------------------------------------------------
p.setPanel = function(size){
	this.panelBackContainer.removeChild(this.panelSprite);
	switch (size){
		case "LL":
			this.panelSprite = this.bg_ll;
			break;
		case "L":
			this.panelSprite = this.bg_l;
			break;
		case "M":
			this.panelSprite = this.bg_m;
			break;	
		case "S":
			this.panelSprite = this.bg_s;
			break;
		case "HELP":
			this.panelSprite = this.bg_help;
			break;
		default:
			this.panelSprite = this.bg_l;
			break;
	}
	this.panelBackContainer.addChild(this.panelSprite);
}

//----------------------------------------------------
//ダイアログ文字
//----------------------------------------------------
p.setText = function(text, chars){
	//onsole.log(text);
	if(!chars) chars = 20;
	this.dialogText.text = breakLines(text, chars);
	//--------------------------------------------------
	//18文字で折り返す。
	//----------------------------------------------------
	function breakLines(_str, chars) {
		var result_str = "";
		var myPattern = /\r|\n|\r\n/g;
		var str_array = _str.split(myPattern);
		var count = str_array.length;
		for (i = 0; i < count; i++) {
		  		str_array[i] =breakSingleLine( str_array[i], chars);
		}
		return str_array.join("\r");
	}
	function breakSingleLine(_str, chars) {
		var lines_str = "";
		var begin = 0;
		var end = _str.length;
		var doIt = true;
		while (doIt) {
			var last = begin + chars - 1;
			if (last <= end) {
				var nextChar = _str.charAt(last + 1);
			  	if (nextChar == "。" || nextChar == "、") {
			    	last--;
			  	}
			  	lines_str += _str.substring(begin, last + 1) + "\r";
			  	begin = last + 1;
			} else {
				lines_str += _str.substring(begin, end + 1);
			  	doIt = false;
			}
		}
		return lines_str;
	}
}
//----------------------------------------------------
// ダイアログを開く
//----------------------------------------------------
p.show = function(callBack, callBackParam, waitTime){
	this.setLayout();
	this.visible = true;
	this.panel.alpha = 0;
	this.panel.scaleX = 0.3;
	this.panel.scaleY = 0.3;
	this.panel.visible = true;
	this.backGround.visible = true;

	this.button_0.setButtonEnabled(true);
	this.button_1.setButtonEnabled(true);

	this.tween = createjs.Tween.get(this.panel);
	this.tween.ignoreGlobalPause  = true;
	_this = this;
	if(callBack){
		this.tween 
	     .wait(waitTime)
	     .to({scaleX:1,scaleY:1, alpha:1}, 500, createjs.Ease.backOut)
	     .wait(200)
	     .call(callBack, [callBackParam]);
	}else{
		this.tween 
	     .wait(waitTime)
	     .to({scaleX:1,scaleY:1, alpha:1}, 500, createjs.Ease.backOut)
	     .wait(200);
	}
}

//----------------------------------------------------
// ダイアログを閉じる
//----------------------------------------------------
p.hide = function(callBack, callBackParam, waitTime){
	var _this = this;
	//this.panel.alpha = 1;
	this.panel.scaleX = 1;
	this.panel.scaleY = 1;
	this.panel.visible = true;
	//this.backGround.visible = true;

	this.button_0.setButtonEnabled(false);
	this.button_1.setButtonEnabled(false);

	this.tween = createjs.Tween.get(this.panel);
	this.tween.ignoreGlobalPause  = true;

	if(callBack){
		this.tween 
			.wait(waitTime)
		    .to({scaleX:0.1,scaleY:0.3, alpha:0}, 300, createjs.Ease.liner )
		    .wait(200)
		    .call(callBack, [callBackParam])
		    .call(hideBackGround)
		    .set({visible:false});
	}else{
		this.tween
			.wait(waitTime)
		    .to({scaleX:0.1,scaleY:0.3, alpha:0}, 200, createjs.Ease.liner )
		    .wait(200)
		    .call(hideBackGround)
		    .set({visible:false});
	}

	function hideBackGround(){
		_this.backGround.visible = false;
	}
}

//==================================================
//ダブルアップダイアログ
//==================================================
cjs.doubleupDialog = function(){
	cjs.Dialog.call(this);
	var _this = this;
	this.setText("ダブルアップに挑戦しますか？");
	this.setPanel("M");

	this.button_0.setStatus("はい", "btn_a", null, {} );
	this.button_1.setStatus("いいえ", "btn_b", null, {} );
	
	this.countTxt = new cjs.Text("残り0回", "bold 32px 'Arial'", "#FFF");
	this.panel.addChild(this.countTxt);

	//this.setCountNum(10);

}
cjs.extend(cjs.doubleupDialog, cjs.Dialog);
var p = cjs.doubleupDialog.prototype;

p.setCountNum = function(num){
	if(num > 0){
		this.setText("ダブルアップに挑戦しますか？");
		this.countTxt.text = "残り" + num +"回";
		this.button_0.visible = true;
		this.button_1.visible = true;
				this.button_0.setLayout("btn_a");
		this.button_0.label.text  = "はい";
		this.button_1.label.text  = "いいえ";
	}else{
		this.setText("ダブルアップ回数の上限に達しました");
		this.countTxt.text = "";
		this.button_0.visible = true;
		this.button_1.visible = false;
		this.button_0.setLayout("btn_c");
		this.button_0.label.text  = "NEW GAME";
		this.button_1.label.text  = "0";
	}
}

p.setLayout = function(){
	var BUTTON_MARGIN = 6;

	this.panelWidth  = this.panelSprite.getBounds().width;
	this.panelHeight = this.panelSprite.getBounds().height;

	this.panel.regX = this.panelWidth/2;
	this.panel.regY = this.panelHeight/2;

	this.panel.x = lib.properties.width/2;
	this.panel.y = lib.properties.height/2 + this.panel_dy;

	this.dialogText.x = (this.panelWidth - this.dialogText.getBounds().width)/2;
	this.dialogText.y = 30;
	
	if(this.countTxt.text != ""){
		this.countTxt.x = (this.panelWidth - this.countTxt.getBounds().width)/2;
	}
	this.countTxt.y = 65;

	this.button_0.y = this.panelHeight - 60;
	this.button_1.y = this.panelHeight - 60;
	
	if(this.button_0.visible && !this.button_1.visible){
		this.button_0.x = this.panelWidth/2;
	}else{
		this.button_0.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_0.width/2;
		this.button_1.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_1.width/2 + this.button_0.width + BUTTON_MARGIN;
	}

}


//==================================================
//メニューダイアログ
//==================================================
cjs.menuDialog = function(game){
	cjs.Dialog.call(this);

	var _this = this;

	this.setPanel("LL")
	
	//this.titleText = new cjs.Text("MENU", "bold 40px 'Arial'", "#FFF");
	//this.panel.addChild(this.titleText);
	this.titleText.text ="MENU";

	this.btn_play = new cjs.DialogButton("ゲームを再開する","btn_c",null, {});
	//this.panel.addChild(this.btn_play);

	this.btn_exit = new cjs.DialogButton("ゲームを終了する","btn_d",showExit, {});
	this.panel.addChild(this.btn_exit);

	if(game =="roulette"){
		this.setText("");
	}else{
		this.setText("ゲームの途中で終了すると、BETしたメダルは返還されません。");
	}
	this.dialogText.font ="bold 28px Arial";
	this.btn_help = new cjs.DialogButton("ヘルプ","btn_c",null, {});
	this.panel.addChild(this.btn_help);
	
	this.btn_addmedal = new cjs.DialogButton("メダルを追加する","btn_d",showPurchase, {});
	//this.panel.addChild(this.btn_addmedal);

	this.btn_close = new cjs.DialogButton("閉じる","btn_b",null, {});
	this.panel.addChild(this.btn_close);

	//ゲーム終了確認ダイアログ
	this.exit_dialog = new createjs.exitDialog(game);
	this.addChild(this.exit_dialog );
	function showExit(){
		_this.exit_dialog.show();
	}

	//メダル確認ダイアログ
	this.purchase_dialog = new createjs.purchaseDialog();
	this.addChild(this.purchase_dialog );
	function showPurchase(){
		_this.purchase_dialog.show();
	}
	this.buttonOnCount = 0;

}
cjs.extend(cjs.menuDialog, cjs.Dialog);
var p = cjs.menuDialog.prototype;

p.setLayout = function(){
	
	this.panelWidth  = this.panelSprite.getBounds().width;
	this.panelHeight = this.panelSprite.getBounds().height;

	this.panel.regX = this.panelWidth/2;
	this.panel.regY = this.panelHeight/2;

	this.panel.x = lib.properties.width/2;
	this.panel.y = lib.properties.height/2 + this.panel_dy;

	this.titleText.x = (this.panelWidth - this.titleText.getBounds().width)/2;
	this.titleText.y = 40;

	this.btn_play.regX = this.btn_play.getBounds().width/2;
	this.btn_play.x =  this.panelWidth/2;
	this.btn_play.y = 140; 

	this.btn_exit.regX = this.btn_exit.getBounds().width/2;
	this.btn_exit.x =  this.panelWidth/2;
	this.btn_exit.y = 240;
	
	if(this.dialogText.text != ""){
		this.dialogText.x = (this.panelWidth - this.dialogText.getBounds().width)/2;
	}
	this.dialogText.y = 280;

	this.btn_help.regX = this.btn_help.getBounds().width/2;
	this.btn_help.x =  this.panelWidth/2;
	this.btn_help.y = 140;

	this.btn_addmedal.regX = this.btn_addmedal.getBounds().width/2;
	this.btn_addmedal.x =  this.panelWidth/2;
	this.btn_addmedal.y = 240;

	this.btn_close.regX = this.btn_close.getBounds().width/2;
	this.btn_close.x =  this.panelWidth/2;
	this.btn_close.y = 440; 
}

p.show = function(){
	this.setLayout();
	this.visible = true;
	this.panel.alpha = 0;
	this.panel.scaleX = 0.3;
	this.panel.scaleY = 0.3;
	this.panel.visible = true;
	this.backGround.visible = true;

	this.tween = createjs.Tween.get(this.panel);
	this.tween.ignoreGlobalPause  = true;
	this.tween.wait(20)
	    .to({scaleX:1,scaleY:1, alpha:1}, 500, createjs.Ease.backOut)
	    .wait(200);
}

p.hide = function(callBack){
	var _this = this;
	//this.panel.alpha = 1;
	//console.log(callBack);
	this.callBack = callBack;
	this.panel.scaleX = 1;
	this.panel.scaleY = 1;
	this.panel.visible = true;
	this.backGround.visible = true;
	//this.dialogText.visible = false;
	//this.titleText.visible = false;
	this.exit_dialog.hide();

	this.tween = createjs.Tween.get(this.panel);
	this.tween.ignoreGlobalPause  = true;
	this.tween.wait(20)
	    .to({scaleX:0.3,scaleY:0.3, alpha:0}, 200, createjs.Ease.liner )
	    .call(hideBackGround)
	    .set({visible:false});
	

	function hideBackGround(){
		_this.backGround.visible = false;
		if(_this.callBack){
			_this.callBack();
		}
	}
}


//==================================================
//　ゲーム終了ダイアログ
//==================================================
cjs.exitDialog =  function(game){
	cjs.Dialog.call(this);
	var _this = this;
	this.titleText.text = "ゲームを終了しますか？"
	if(game == "roulette"){
		this.setText("ゲームを途中で終了すると、獲得したメダルは進呈されませんのでご注意ください。", 25);
	}else{
		this.setText("ゲームを途中で中断すると消費したメダルは返還されません。", 25);
	}
	this.dialogText.font = "bold 24px Arial" 
	this.button_0.setStatus("はい", "btn_a", null, {});
	this.button_1.setStatus("いいえ", "btn_b", null, {});
}
cjs.extend(cjs.exitDialog, cjs.Dialog);
var p = cjs.exitDialog.prototype;


p.setLayout = function(){
	this.panelWidth  = this.panelSprite.getBounds().width;
	this.panelHeight = this.panelSprite.getBounds().height;

	this.panel.regX = this.panelWidth/2;
	this.panel.regY = this.panelHeight/2;

	this.panel.x = lib.properties.width/2;
	this.panel.y = lib.properties.height/2 + this.panel_dy;

	this.titleText.x = (this.panelWidth - this.titleText.getBounds().width)/2;
	this.titleText.y = 30;

	this.dialogText.x = (this.panelWidth - this.dialogText.getBounds().width)/2;
	this.dialogText.y = 175;
	
	var BUTTON_MARGIN = 6;

	this.button_0.y = 120;
	this.button_1.y = 120;
	this.button_0.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_0.width/2;
	this.button_1.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_1.width/2 + this.button_0.width + BUTTON_MARGIN;
}


//==================================================
//　メダル購入ダイアログ
//==================================================
cjs.purchaseDialog =  function(){
	cjs.Dialog.call(this);
	var _this = this;
	this.titleText.text = "ゲームを終了してメダルを追加しますか？"
	this.titleText.font = "bold 32px Arial" 
	this.setText("ゲームを途中で終了すると消費したメダルは返還されません。また、ゲームで獲得したメダルは進呈されませんのでご注意ください。", 25);
	this.dialogText.font = "bold 24px Arial" 
	this.button_0.setStatus("はい", "btn_a", null, {});
	this.button_1.setStatus("いいえ", "btn_b", null, {});
}
cjs.extend(cjs.purchaseDialog, cjs.Dialog);
var p = cjs.purchaseDialog.prototype;

p.setLayout = function(){
	this.panelWidth  = this.panelSprite.getBounds().width;
	this.panelHeight = this.panelSprite.getBounds().height;

	this.panel.regX = this.panelWidth/2;
	this.panel.regY = this.panelHeight/2;

	this.panel.x = lib.properties.width/2;
	this.panel.y = lib.properties.height/2 + this.panel_dy;

	this.titleText.x = (this.panelWidth - this.titleText.getBounds().width)/2;
	this.titleText.y = 30;

	this.dialogText.x = (this.panelWidth - this.dialogText.getBounds().width)/2;
	this.dialogText.y = 175;
	
	var BUTTON_MARGIN = 6;

	this.button_0.y = 120;
	this.button_1.y = 120;
	this.button_0.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_0.width/2;
	this.button_1.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_1.width/2 + this.button_0.width + BUTTON_MARGIN;
}


//==================================================
//　エラーダイアログ
//==================================================
cjs.errorDialog = function(game){
	cjs.Dialog.call(this);
	var _this = this;
	this.setPanel("L");
	this.codeText = new cjs.Text("", "bold 28px 'Arial'", "#FFF");
	this.panel.addChild(this.codeText);	

	//ゲーム終了確認ダイアログ
	this.exit_dialog = new createjs.exitDialog(game);
	this.addChild(this.exit_dialog );
}
cjs.extend(cjs.errorDialog, cjs.Dialog);
var p = cjs.errorDialog
.prototype;
p.setLayout = function(){
	this.panelWidth  = this.panelSprite.getBounds().width;
	this.panelHeight = this.panelSprite.getBounds().height;

	this.panel.regX = this.panelWidth/2;
	this.panel.regY = this.panelHeight/2;

	this.panel.x = lib.properties.width/2;
	this.panel.y = lib.properties.height/2 + this.panel_dy;

	var BUTTON_MARGIN = 6;

	var dialogTextHeight = this.dialogText.text != "" ? this.dialogText.getBounds().height : 0;
	var codeTextHeight = this.codeText.text != "" ? this.dialogText.getBounds().height : 0;
	
	if(this.dialogText.text != ""){
		this.dialogText.x = (this.panelWidth - this.dialogText.getBounds().width)/2;
		this.dialogText.y = 40;
	}

	if(this.codeText.text != ""){	
		this.codeText.x = (this.panelWidth - this.codeText.getBounds().width)/2;
		this.codeText.y = this.dialogText.y  + 100;
	}

	if(this.button_0.visible && !this.button_1.visible || !this.button_0.visible && this.button_1.visible){

		this.button_0.x = this.panelWidth/2;
		this.button_0.y = this.panelHeight - 60;
		this.button_1.x = this.panelWidth/2;
		this.button_1.y = this.panelHeight - 60;
	}else if(this.button_0.visible && this.button_1.visible){

		if(this.button_0.width + this.button_1.width > this.panelWidth ){
			//縦に並べる
			this.button_0.x = this.panelWidth/2;
			this.button_0.y = this.panelHeight - 120
			this.button_1.x = this.panelWidth/2;
			this.button_1.y = this.panelHeight - 50;

		}else{
			this.button_0.y = this.panelHeight - 60;
			this.button_1.y = this.panelHeight - 60;
			this.button_0.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_0.width/2;
			this.button_1.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_1.width/2 + this.button_0.width + BUTTON_MARGIN;

		}
	}
}


//==================================================
//　メンテダイアログ
//==================================================
cjs.maintenanceDialog = function(){
	cjs.Dialog.call(this);
	var _this = this;
	this.setPanel("LL");
	this.setText("")
	this.titleText = new cjs.Text("只今メンテナンス中です", "bold 32px 'Arial'", "#FFF");
	this.panel.addChild(this.titleText);
	this.textBox = new cjs.textBox(560, 320);
	this.panel.addChild(this.textBox);
	this.button_0.setStatus("ゲームを終了する", "btn_c", null, {});
}
cjs.extend(cjs.maintenanceDialog, cjs.Dialog);
var p = cjs.maintenanceDialog.prototype;

p.setLayout = function(){
	this.panelWidth  = this.panelSprite.getBounds().width;
	this.panelHeight = this.panelSprite.getBounds().height;

	this.panel.regX = this.panelWidth/2;
	this.panel.regY = this.panelHeight/2;

	this.panel.x = lib.properties.width/2;
	this.panel.y = lib.properties.height/2 + this.panel_dy;
	
	var BUTTON_MARGIN = 6;

	if(this.button_0.visible && !this.button_1.visible || !this.button_0.visible && this.button_1.visible){
		this.button_0.x = this.panelWidth/2;
		this.button_0.y = this.panelHeight - 60;
		this.button_1.x = this.panelWidth/2;
		this.button_1.y = this.panelHeight - 60;
	}else if(this.button_0.visible && this.button_1.visible){

		if(this.button_0.width + this.button_1.width > this.panelWidth ){
			//縦に並べる
			this.button_0.x = this.panelWidth/2;
			this.button_0.y = this.panelHeight - 120
			this.button_1.x = this.panelWidth/2;
			this.button_1.y = this.panelHeight - 50;
		}else{
			this.button_0.y = this.panelHeight - 60;
			this.button_1.y = this.panelHeight - 60;
			this.button_0.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_0.width/2;
			this.button_1.x = (this.panelWidth - this.button_0.width - this.button_1.width - BUTTON_MARGIN)/2 + this.button_1.width/2 + this.button_0.width + BUTTON_MARGIN;

		}
	}
	
	this.textBox.x = (this.panelWidth - this.textBox.width)/2;
	this.textBox.y = 80;

	if(this.titleText.text){	
		this.titleText.x = (this.panelWidth - this.titleText.getBounds().width)/2;
		this.titleText.y = 25;
	}
}

//==================================================
//　テキストボックス
//==================================================
cjs.textBox = function(width, height){
	cjs.Container.call(this);
	
	var _this = this;

	this.MARGIN_X = 6;
	this.MARGIN_Y = 6;

	this.height = height;
	this.width = width;

	this.box = new cjs.Shape();
	this.box.graphics.setStrokeStyle(1);
	this.box.graphics.beginStroke("#FFFFFF");
	this.box.graphics.beginFill("000000");
	this.box.graphics.drawRect(0,0,this.width - 20,this.height);
    this.box.width = this.width - 20;


	this.scrollBarFrame = new cjs.Shape();
	this.scrollBarFrame.graphics.setStrokeStyle(1);
	this.scrollBarFrame.graphics.beginStroke("#FFFFFF");
	this.scrollBarFrame.graphics.drawRect(0,0,16,this.height);
	this.scrollBarFrame.x = this.width - 16;

	this.scrollBar = new cjs.Shape();
	this.scrollBar.graphics.beginFill("#FFFFFF");
	this.scrollBar.graphics.drawRect(0,0,12,this.height - 4);
	this.scrollBar.x = this.width - 14;
	this.scrollBar.y = 2;
	this.scrollBar.height = this.height - 4;

	this.boxText = new cjs.Text("", "bold 24px 'Arial'", "#FFF");
	this.boxText.mask = this.box;
	this.boxText.x = this.MARGIN_X; 
	this.boxText.y = this.MARGIN_Y;

	this.addChild(this.box);
	this.addChild(this.scrollBarFrame);
	this.addChild(this.scrollBar);
    this.addChild(this.boxText);
}
cjs.extend(cjs.textBox, cjs.Container);
var p = cjs.textBox.prototype;
p.setText = function(text, chars){

	var _this = this; 
	if(!chars) chars = 22;
	this.boxText.text = breakLines(text, chars);
	//--------------------------------------------------

	//----------------------------------------------------
	function breakLines(_str, chars) {
		var result_str = "";
		var myPattern = /\r|\n|\r\n/g;
		var str_array = _str.split(myPattern);
		var count = str_array.length;
		for (i = 0; i < count; i++) {
		  	str_array[i] =breakSingleLine( str_array[i], chars);
		}
		return str_array.join("\r");
	}
	function breakSingleLine(_str, chars) {
		var all_str = "";
		var line_str = "";
		var str = _str.split("");
		var counter = 0;
		var dummyText = new cjs.Text("", "bold 24px 'Arial'", "#FFF");

		for(var i=0; i<str.length; i++){
			line_str += str[i];
			all_str += str[i];
			dummyText.text = line_str;
			if((dummyText.getBounds().width > _this.box.width-30)&&(i < str.length-1)){
				all_str +=  "\n";
				line_str ="";
			}
		}
		return all_str;

	}

	var boxTextHeight = this.boxText.getBounds().height;
	if(boxTextHeight > this.height){
		this.scrollBar.visible = true;
	    this.scrollBar.scaleY = this.height / boxTextHeight;
	    this.scrollBar.height =	this.scrollBar.height * this.scrollBar.scaleY;
	    this.box.on('mousedown',  mouseDownHandler, null, false);
		this.box.on('pressmove',  pressMoveHandler, null, false);
	}else{
		this.scrollBar.visible = false;
		this.box.off('mousedown',  mouseDownHandler, null, false);
		this.box.off('pressmove',  pressMoveHandler, null, false);
	}

	function mouseDownHandler(event){
		mouseY = event.stageY;
	}

	function pressMoveHandler(event){
		var dy = event.stageY - mouseY;
		mouseY = event.stageY;

		_this.boxText.y += dy;
		_this.boxText.mask = _this.frameShape;

		if(_this.boxText.y > _this.MARGIN_X) _this.boxText.y = _this.MARGIN_X;

		var dialogTextBottomY = _this.boxText.y + _this.boxText.getBounds().height;
		if(dialogTextBottomY < _this.height + _this.MARGIN_Y )  _this.boxText.y = _this.height  - _this.MARGIN_Y  - _this.boxText.getBounds().height 
		_this.boxText.mask = _this.box;
		_this.scrollBar.y = 4 +  (_this.height - _this.scrollBar.height - 8) *  (-_this.boxText.y / (_this.boxText.getBounds().height - _this.height));
	}
}




//==================================================
//　ダイアログ用ボタン
//==================================================
cjs.DialogButton = function(label,type,callBack,callBackParam){
	cjs.MovieClip.call(this);


	this.btn_a = new cjs.Sprite(ssDialog, "btn_a");
	this.btn_b = new cjs.Sprite(ssDialog, "btn_b");
	this.btn_c = new cjs.Sprite(ssDialog, "btn_c");
	this.btn_d = new cjs.Sprite(ssDialog, "btn_d");

	var button_bg,button_width,button_color;

	this.buttonBackContainer = new cjs.Container();
	this.buttonSprite = this.btn_a;
	this.buttonBackContainer.addChild(this.buttonSprite);
	this.addChild(this.buttonBackContainer);


	this.bounds = this.buttonSprite.getBounds();
	this.width = this.buttonSprite.getBounds().width;
	this.height = this.buttonSprite.getBounds().height;
	this.regX = this.width/2;
	this.regY = this.height/2;

	this.label = new cjs.Text(label,"bold 32px 'Arial'","#FFF");
	this.label.textAlign ="center";
	this.label.textBaseline = "middle";
	this.label.x = this.width/2;
	this.label.y = this.height/2;

	this.callBack = callBack;
	this.callBackParam = callBackParam;

	//リスナー用のシェイプとそのヒットエリアを作る
	this.hitShape = new cjs.Shape();
	this.hitAreaShape = new cjs.Shape();
	this.hitAreaShape.graphics.beginFill("rgba(255,0,0,1)");
	this.hitAreaShape.graphics.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
	this.hitShape.hitArea = this.hitAreaShape;

	this.addChild(this.buttonBackContainer);
	this.addChild(this.label);
	this.addChild(this.hitShape);
	var data ={};
	this.hitShape.on("mousedown", mousedownHandler, null, false, data);
	this.hitShape.on("pressup", pressupHandler, null, false, data);
	
	var _this = this;
	function mousedownHandler(event){
		//console.log(_this.parent.parent.buttonOnCount);
		//_this.parent.parent.buttonOnCount ++;
	    if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchstart") {
            // mousedownも発生する機種対策で、touchstart以外は処理しない
            return;
        }
		_this.scaleX = 0.9;
		_this.scaleY = 0.9;
	}

	var waitTimer = 0;
	function pressupHandler(event){
		if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchend") {
            // mouseupも発生する機種対策で、touchend以外は処理しない
            return;
        }
		_this.scaleX = 1;
		_this.scaleY = 1;

		if(waitTimer == 0){
			//console.log(_this.callBack);
			_this.callBack(_this.callBackParam)
			wait();
		}
	}
	function wait(){
		createjs.Ticker.addEventListener('tick', tickHandler);
		function tickHandler(event){
			waitTimer++;
			if(waitTimer > 3){
				waitTimer = 0;
				createjs.Ticker.removeEventListener('tick', tickHandler);
			}
		}
	}
	this.setStatus(label, type, callBack, callBackParam);
}
cjs.extend(cjs.DialogButton, cjs.MovieClip);


var p = cjs.DialogButton.prototype;


//ボタン設定
p.setStatus = function(label, type, callBack, callBackParam){
	this.label.text = label;
	this.callBack = callBack;
	this.callBackParam = callBackParam;
	this.setLayout(type);
	this.visible = true;
}

//ボタン設定
p.setFunc = function(callBack, callBackParam){
	this.callBack = callBack;
	this.callBackParam = callBackParam;
}

p.setLayout = function(type){
	this.buttonBackContainer.removeChild(this.buttonSprite);
	switch(type){
		case "btn_a":
			this.buttonSprite = this.btn_a;
			break;
		case "btn_b":
			this.buttonSprite = this.btn_b;
			break;
		case "btn_c":
			this.buttonSprite = this.btn_c;
			break;
		case "btn_d":
			this.buttonSprite = this.btn_d;
			break;
		case "btn_e":
			this.buttonSprite = this.btn_e;
			break;
		default:
			this.buttonSprite = btn_a;
			break;
	}
	this.buttonBackContainer.addChild(this.buttonSprite);

	this.bounds = this.buttonSprite.getBounds();
	this.width = this.buttonSprite.getBounds().width;
	this.height = this.buttonSprite.getBounds().height;
	this.regX = this.width/2;
	this.regY = this.height/2;
	this.label.x = this.width/2;
	this.label.y = this.height/2;

	this.hitAreaShape.graphics.clear();
	this.hitAreaShape.graphics.beginFill("rgba(255,0,0,1)");
	this.hitAreaShape.graphics.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
}

p.setButtonEnabled = function(flag){
	if(flag){
		this.mouseEnabled = true;
		this.label.color = "#FFF";
	}else{
		this.mouseEnabled = false;
		this.label.color = "#666";

	}
}

//==================================================
//メダルゲット用ダイアログ
//==================================================
cjs.DialogWin = function(data){
	//console.log(data)
	cjs.Container.call(this);
	this.max = data.max ? data.max : 99999999;
	this.dx = data.dx ? data.dx : 0;
	this.dy = data.dy ? data.dy : 0;
	this.visible = false;
	this.width = 636;
	this.height = 253;
	this.regX = this.width/2;
	this.regY = this.height/2;

	this.textMargin = 10;
	var textY = 153;

	this.x = lib.properties.width/2 + this.dx;
	this.y = lib.properties.height/2 + this.dy;

	var param ={align:"left"};
	this.imageNumber = new cjs.ImageNumber("../lib/images/win_number.png?"+version,36,54,param);
	this.imageNumber.y = textY;
	this.imageNumberWidth = 36;
	
	this.medalGetImage = new cjs.Bitmap("../lib/images/win_get.png?"+version);
	this.medalGetImage.y = textY;
	this.medalGetImageWidth = 303;

	var backGroundImage = new cjs.Bitmap("../lib/images/win.png?"+version);

	//----------------------------------------------------
	// ヒットエリアを用意する
	//----------------------------------------------------
	var hitAreaShape = new createjs.Shape();
	hitAreaShape.graphics.beginFill("#000000").drawRect(0, 0, this.width , this.height);
	this.hitArea = hitAreaShape;

	this.addChild(backGroundImage);
	this.addChild(this.imageNumber);
	this.addChild(this.medalGetImage);
}
cjs.extend(cjs.DialogWin, cjs.Container);
var p = cjs.DialogWin.prototype;
p.show = function(number, callBack, callBackParam, waitTime){

	this.alpha = 0;
	this.scaleX = 0.3;
	this.scaleY = 0.3;
	//数字をセットしてセンタリングする
	if(number && this.max){
		if(number > this.max){
			number = this.max;
		}
	}
	this.visible = true;
	this.imageNumber.setNumber(number);
	var numbersLength =  number.toString().length;
	var textWidth =  this.imageNumberWidth * numbersLength + this.medalGetImageWidth + this.textMargin;

	var leftX = (this.width - textWidth)/2;
	this.imageNumber.x = leftX
	this.medalGetImage.x = leftX + numbersLength * this.imageNumberWidth + this.textMargin;

	createjs.Tween.get(this)
     .wait(waitTime)
     .to({scaleX:1,scaleY:1, alpha:1}, 300, createjs.Ease.backOut)
     .wait(200)
     .call(callBack, [callBackParam]);
}

p.hide = function(callBack, callBackParam, waitTime){
	if(callBack){
		createjs.Tween.get(this)
			.wait(waitTime)
		    .to({scaleX:0.3,scaleY:0.3, alpha:0}, 300, createjs.Ease.liner )
		    .wait(200)
		    .call(callBack, [callBackParam])
		    .set({visible:false});
	}else{
		createjs.Tween.get(this)
			.wait(waitTime)
		    .to({scaleX:0.3,scaleY:0.3, alpha:0}, 200, createjs.Ease.liner )
		    .wait(200)
		    .set({visible:false});
	}
}

})();