//コンソールログ出力用
_log = function(){};
if(CI_ENV !== "production"){
	//_log = function(){console.log.bind("data", "%c%s", "color:blue;font-weight:bold;")};
	_log = console.log.bind("data", "%c%s", "color:blue;font-weight:bold;");
}

this.cf = this.cf||{};
this.cjs = createjs;
(function(){

//戻り先URL
cf.back_url = document.referrer ? window.location.href : document.referrer;
//ゲームID
cf.game_id ="";

//------------------------------------------------
//　stageの代わりに使うShape 
//　最上位レイヤーにおいて使用する
//------------------------------------------------
cf.stage = new createjs.Shape();
var hitShape = new createjs.Shape();
hitShape.graphics.beginFill("rgba(0,0,0,1)");
hitShape.graphics.drawRect(0,0,lib.properties.width, lib.properties.height);
cf.stage.hitArea = hitShape;



//------------------------------------------------
//　親フレームのスクロール対応
//------------------------------------------------
var dragPointY = 0;
cf.scrollStart = function(event){
	dragPointY = stage.mouseY;
}
var dragcounter = 0;
var sRatio = 1;
cf.scrollMove　= function(event){
	dragcounter++;
	if(dragcounter%2 !== 0) return;
	var diffY = stage.mouseY - dragPointY;
	//if(diffY > 300 || diffY < -300) return;
	//dragPointY = stage.mouseY;
	cf.postMessage(diffY *sRatio);
}

cf.scrollEnd　= function(event){
	dragPointY = 0;
}

//親フレームへのpostMessage
cf.postMessage = function(data){
	if(document.referrer){
		//_log("postMessage",  data);
		window.parent.postMessage(data, document.referrer);
	}
}
//------------------------------------------------
//画面を隠すマスク
//------------------------------------------------
cf.mask = new createjs.Shape();
cf.mask.graphics.beginFill("rgba(0,0,0,1)");
cf.mask.graphics.drawRect(0,0,lib.properties.width, lib.properties.height);
cf.mask.hitArea = hitShape;
cf.mask.addEventListener("click", function(){});

//------------------------------------------------
//ローディング
//------------------------------------------------
cf.loading = new createjs.Loading();

cf.show_loading = function(){
	cf.mask.visible = true;
	cf.mask.alpha = 0.5;
	cf.loading.show();
}

cf.hide_loading = function(){
	cf.mask.visible = false;
	cf.loading.hide();
}
//------------------------------------------------
// メダルゲット文字
//------------------------------------------------
//cf.medalGetText = new createjs.MedalGetText();


//------------------------------------------------
//　ポーズ機能
//------------------------------------------------
cf.pauseFlag = false;
cf.helpFlag = false;
//一時停止
cf.pause = function(){
	createjs.Ticker.setPaused(true);
}
//再生開始
cf.play = function(){
	if(cf.pauseFlag || cf.helpFlag){
		return;
	}
	createjs.Ticker.setPaused(false);
}

//------------------------------------------------
//メニューダイアログ
//------------------------------------------------
cf.menu_dialog =  new createjs.menuDialog();

cf.show_menu = function(){
	cf.pause();
	cf.pauseFlag  = true;
	cf.menu_dialog.show();
}

cf.hide_menu = function(){
	cf.pauseFlag  = false;
	cf.menu_dialog.exit_dialog.hide();
	cf.menu_dialog.hide(cf.play);
}

cf.show_help = function(){
	cf.pauseFlag  = false;
	cf.helpFlag = true;
	cf.pause();
	cf.menu_dialog.exit_dialog.hide();
	cf.menu_dialog.hide(
		function(){
			exportRoot.mc_help.visible = true;
			exportRoot.mc_help.showPage(1,1);
			}
		);
	cf.pauseFlag  = false;
}

cf.hide_help = function(){
	cf.helpFlag = false;
	exportRoot.mc_help.visible = false;
	cf.play();
}

cf.exit = function(){
	cf.jump(cf.back_url, {}, null, null );
}

cf.hide_exit = function(){
	cf.menu_dialog.exit_dialog.hide();
}

cf.menu_dialog.btn_help.setFunc(cf.show_help);
cf.menu_dialog.btn_play.setFunc(cf.hide_menu);
cf.menu_dialog.btn_close.setFunc(cf.hide_menu);

cf.menu_dialog.exit_dialog.button_0.setFunc(cf.exit);
cf.menu_dialog.exit_dialog.button_1.setFunc(cf.hide_exit);

//------------------------------------------------
//画面を横向けたときの処理
//------------------------------------------------
function show_landscape_dialog(){
	var alert_mask_div = document.getElementById("alert_mask");
	var alert_text_div = document.getElementById("alert_text");

	alert_mask_div.style.display = "block";
	alert_text_div.textContent = "画面を縦にしてください"
    cf.pause();
} 


function hide_landscape_dialog(){
	var alert_mask_div = document.getElementById("alert_mask");
	var alert_text_div = document.getElementById("alert_text");
	alert_mask_div.style.display = "none";
    cf.play();
} 


//------------------------------------------------
//画面のリサイズ
//------------------------------------------------
var lastW,lastH,lastS;
cf.resizeCanvas = function() {

	var w = lib.properties.width, h = lib.properties.height;
	var iw = window.innerWidth, ih=window.innerHeight;

	iw = document.documentElement.clientWidth;
	ih = document.documentElement.clientHeight;
	var cw = document.documentElement.clientWidth;
	var ch = document.documentElement.clientHeight;

	//alert("iw:" + iw + ",cw:" + cw);
	if(iw > cw){
		//alert("iw:" + iw + ",cw:" + cw);
		iw = cw;
	}
	if(ih > ch) ih = ch;
 	var pRatio = window.devicePixelRatio, xRatio=iw/w, yRatio=ih/h;
 	//var pRatio = window.devicePixelRatio, xRatio=pw/w, yRatio=ph/h, sRatio=1;
 	/*
 	alert("resizeCanvas iw:" + iw + ",ih:" + ih);
 	alert("resizeCanvas cw:" + cw + ",ch:" + ch);
 	alert("resizeCanvas pw:" + pw + ",ph:" + h);
 	*/
	sRatio = Math.min(xRatio, yRatio);
	canvas.style.width  = w*sRatio+'px';			
	canvas.style.height  = h*sRatio+'px';
	lastW = iw; lastH = ih; lastS = sRatio;
}


//------------------------------------------------
//画面横向けた時の処理
//------------------------------------------------
// 初期化処理
//window.addEventListener('load', checkOrientation, false);

var iPhoneFlag = navigator.userAgent.match(/(iPhone|iPod|iPad)/) ;
if(iPhoneFlag){
	window.addEventListener('resize', checkOrientation, false);
}else{
	window.addEventListener('resize', checkOrientation, false);
}


function checkOrientation () {
	//alert("checkOrientation");
	if('orientation' in window) {
		var canvas = document.getElementById("canvas");
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;					
		sRatio = Math.min(xRatio, yRatio);
		if(iw < ih){
 			hide_landscape_dialog();
		   	canvas.style.width = w*sRatio+'px';			
			canvas.style.height = h*sRatio+'px';
		}else{
			show_landscape_dialog();
		}
	}
};

//------------------------------------------------
//エラーダイアログ
//------------------------------------------------
cf.error_dialog = new createjs.errorDialog();

cf.error_exit = function(){	
	cf.jump(cf.back_url);
}

cf.error_hide_exit = function(){
	cf.error_dialog.exit_dialog.hide();
}

cf.error_dialog.exit_dialog.button_0.setFunc(cf.error_exit);
cf.error_dialog.exit_dialog.button_1.setFunc(cf.error_hide_exit);

//------------------------------------------------
//　通信エラーダイアログ
//------------------------------------------------
cf.connect_error_dialog = new createjs.errorDialog();

//通信エラー
cf.show_connect_error_dialog = function(reconnect_func, url, param, success_func, error_func){
	//console.log("show_connect_error_dialog:" + success_func )

	cf.connect_error_dialog.setPanel("L");

	cf.connect_error_dialog.setText("ゲームに問題が発生しました　時間をおいてから再度アクセスしてください");
	cf.connect_error_dialog.button_0.setStatus("リトライ", "btn_a", reconnect, {} );
	cf.connect_error_dialog.button_1.setStatus("ゲームを終了する", "btn_d", give_up, {} );
	
	function reconnect(){
		cf.connect_error_dialog.button_0.setButtonEnabled(false);
		cf.connect_error_dialog.button_1.setButtonEnabled(false);
		cf.connect_error_dialog.hide(function(){
					reconnect_func(url, param, success_func, error_func);
				});
	}

	function show_exit(){
		cf.connect_error_dialog.exit_dialog.show();
	}

	function give_up(){
		if(error_func){
			error_func();
		}
		cf.connect_error_dialog.hide();
	}

	cf.connect_error_dialog.show();
}

//レスポンスエラー
cf.show_responce_error_dialog = function(error_type, error_message){
	cf.error_dialog.setPanel("L");
	cf.error_dialog.setText(error_message);
	cf.error_dialog.codeText.text = "ERR " + error_type;
	cf.error_dialog.button_0.setStatus("ゲームを終了する", "btn_c", cf.error_exit , {} );
	cf.error_dialog.show();
}

//レスポンスエラー
cf.show_maintenance_dialog = function(message){
	cf.maintenance_dialog.textBox.setText(message);
	cf.maintenance_dialog.show();
}


//------------------------------------------------
//メンテダイアログ
//------------------------------------------------
cf.maintenance_dialog = new createjs.maintenanceDialog();

//------------------------------------------------
// 初期化
//------------------------------------------------

var compFile = 0;
var manifestLength;
cf.init = function() {
	canvas = document.getElementById("canvas");
	images = images||{};
	ss = ss||{};
	var loader = new createjs.LoadQueue(false);
	loader.addEventListener("fileload", handleFileLoad);
	loader.addEventListener("complete", handleComplete);
	loader.addEventListener("error", handleError);
	manifestLength = lib.properties.manifest.length;
	for(var i=0; i<manifestLength; i++){
		lib.properties.manifest[i].src += ("?" + version);
	}
	loader.loadManifest(lib.properties.manifest);
}

function handleError(evt){
	alert("ロードに失敗しました");
	//console.log(evt);
}

function handleFileLoad(evt) {
	if (evt.item.type == "image") { images[evt.item.id] = evt.result; }	
	compFile ++;
	var progress = parseInt(compFile / manifestLength * 100);
	var progress_div =  document.getElementById("progress");
}

function handleComplete(evt) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	var queue = evt.target;
	var ssMetadata = lib.ssMetadata;
	for(i=0; i<ssMetadata.length; i++) {
		ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
	}
	var preloaderDiv = document.getElementById("_preload_div_");
	preloaderDiv.style.display = 'none';
	canvas.style.display = 'block';
	makeExportRoot();
	
	stage = new createjs.Stage(canvas);
	stage.addChild(exportRoot);
	stage.enableMouseOver();
	if(createjs.Touch.isSupported() == true){
	    // タッチ操作を有効にします。
	    createjs.Touch.enable(stage, true);
	}
	createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage);	    
	window.addEventListener('resize', cf.resizeCanvas);
	cf.resizeCanvas();	
}

//------------------------------------------------
//　api接続
//------------------------------------------------
cf.api = function(url, param, success_func, error_func){
	cf.show_loading();
	send_data(null,null);

	//データ送信
	function send_data(){
		//paramの暗号化
		$.ajax({
				url: "/" + url,
				type:"POST",
				datatype: "json",
				cache: false,
				data: param,
				timeout:20000,
		}).done(function(data, textStatus, jqXHR){
			//console.log(data);
			cf.hide_loading();
			data = JSON.parse(data);
			if(data.meta.code == 200){
				_log("data" ,data);
				success_func(data);
			}else{
				cf.show_responce_error_dialog(data.meta.error_type, data.meta.error_message);
			}
		}).fail(function(data, textStatus, errorThrown){
			cf.hide_loading();
			cf.show_responce_error_dialog('0000', '接続に失敗しました');
		}).always(function(data, textStatus, returnedObject){

		});
	}
}
//------------------------------------------------
//　ページ遷移
//------------------------------------------------
cf.jump = function(url, param, success_func, error_func){
	cf.show_loading();
	if(url){
		parent.location.href = url;
		setTimeout(function(){
			cf.hide_loading();
			cf.show_connect_error_dialog(cf.jump, url, param, success_func, error_func);
		},10000);
	}else{
		//console.log(document.referrer);
		window.history.back(-1);
	}
};

// Cookie取得 引数に Cookieの名前
cf.getCookie = function(key){
  var cookies = document.cookie.split('; ');
  for (var i = 0; i < cookies.length; i++){
    var c = cookies[i].split('=');
    if(c[0] == key){
      return c[1];
    }
  }
  return false;
}

function setMedalCookie(num){
    // クッキーに保存
    document.cookie ='medal=' + num;
    //alert(cf.getCookie('medal'));
}

cf.reloadCheck = function () {
    var flag = window.name == window.location.href ? true : false;
    window.name = window.location.href;
    return flag;
}

//暗号化ファイル
document.write("<script type='text/javascript' src='../lib/cryptojs/aes.js'></script>");

})();