(function(){
var cjs = createjs;
//===============================================================
// ローディングドット
//===============================================================
cjs.LoadingDot = function(){
	cjs.Shape.call(this);
	var g = this.graphics;
	g.beginFill();
	g.beginStroke("#FFFFFF");
	g.setStrokeStyle(5,1,1);
	g.moveTo(20, 0);
	g.lineTo(30, 0);
}
cjs.extend(cjs.LoadingDot, cjs.Shape);
var p = cjs.LoadingDot.prototype;

//===============================================================
// ローディングドット
//===============================================================
cjs.Loading = function() {
	cjs.Container.call(this);
	this.x = lib.properties.width/2;
	this.y = lib.properties.height/2;
	this.dotArr = [];

	for(var i=0; i<12; i++){
		this.dotArr[i] = new cjs.LoadingDot();
		this.dotArr[i].rotation = 30 * i;
		this.dotArr[i].alpha = 0.1 * i;
		//if(this.dotArr[i].alpha < 0.3) this.dotArr[i].alpha = 0.3;
		//if(this.dotArr[i].alpha > 0.9) this.dotArr[i].alpha = 0.9;
		this.addChild(this.dotArr[i]);
	}
	this.listener = null;
	this.visible = false;
}
cjs.extend(cjs.Loading, cjs.Container);
var p = cjs.Loading.prototype;

p.show = function(){
	var _this = this;
	this.visible = true;
	this.listener = cjs.Ticker.addEventListener("tick", rotate);
	var counter = 0;
	function rotate(event){
		counter++;
		_this.rotation = 30 * parseInt(counter / 1);
	}

} 

p.hide = function(){
	cjs.Ticker.removeEventListener("tick", this.listener);
	this.visible = false;
}
})();