
(function(){
var cjs = createjs;
var version ="20170222";
//画像文字クラス
/*================================
path		:スプライトシートの画像パス
textWidth	:1文字の幅
textHeight  :1文字の高さ
data baseX  :基準となるx位置
data baseY  :基準となるy位置
data default:初期値
data max	:最大値
data align	:文字揃え(left, center ,right)
=================================*/
cjs.ImageNumber = function(path,textWidth,textHeight,data){
	cjs.BitmapText.call(this);

	this.textWidth = textWidth;
	this.textHeight = textHeight;
	this.baseX = data.baseX ? data.baseX : 0;
	this.baseY = data.baseY ? data.baseY : 0;
	this.dafault = data.default ? data.default : 0;
	this.align = data.align ? data.align : "left";
	this.max = data.max ? data.max : 99999999;

	var param = {
			images: [path + "?" + version],
			frames: {width:textWidth, height:textHeight, count:10},
			animations: {"0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9}
		} 
	this.spriteSheet = new cjs.SpriteSheet(param);
	this.y = this.baseY;
	this.setNumber(this.dafault);
	
}
cjs.extend(cjs.ImageNumber, cjs.BitmapText );
var p = cjs.ImageNumber.prototype;

//数字を表示する
p.setNumber = function(number){
	if(number && this.max){
		if(number > this.max){
			number = this.max;
		}
	}
	this.text = number.toString();
	switch(this.align){
		case "left":
			this.x = this.baseX;
			break;
		case "center":
			var textLength = this.text.length;
			this.x = this.baseX - (textLength * this.textWidth)/2;
			break;
		case "right":
			var textLength = this.text.length;
			this.x = this.baseX - textLength * this.textWidth;
			break;
		default:
			break;
	}
}
})();