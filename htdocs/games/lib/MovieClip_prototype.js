//-------------------------------------------------------------------------------------
//ムービークリップクラスの拡張
//-------------------------------------------------------------------------------------
//ムービークリップをボタン化する。
createjs.MovieClip.prototype.addButtonFunc = function(func, data){
	this.bounds = this.getBounds();
	var _this = this;

	//リスナー用のシェイプとそのヒットエリアを作る
	this.hitShape = new createjs.Shape();
	var hitAreaShape = new createjs.Shape();
	hitAreaShape.graphics.beginFill("rgba(255,0,0,1)");
	hitAreaShape.graphics.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
	this.hitShape.hitArea = hitAreaShape;
	this.addChild(this.hitShape);
	
	this.hitShape.on("mousedown", mousedownHandler, null, false, data);
	this.hitShape.on("pressup", pressupHandler, null, false, data);

	function mousedownHandler(event){
	    if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchstart") {
            // mousedownも発生する機種対策で、touchstart以外は処理しない
            return;
        }
		_this.scaleX = 0.9;
		_this.scaleY = 0.9;
	}

	var waitTimer = 0;
	function pressupHandler(event){
		if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchend") {
            // mouseupも発生する機種対策で、touchend以外は処理しない
            return;
        }
		_this.scaleX = 1;
		_this.scaleY = 1;
		if(waitTimer == 0){
			func(data);
			wait();
		}

		function wait(){
			createjs.Ticker.addEventListener('tick', tickHandler);
			function tickHandler(event){
				waitTimer++;
				if(waitTimer > 3){
					waitTimer = 0;
					createjs.Ticker.removeEventListener('tick', tickHandler);
				}
			}
		}
	}
}



//ボタンについているファンクションを変更する
createjs.MovieClip.prototype.changeButtonFunc = function(func, data){
	var _this = this;
	this.hitShape.removeAllEventListeners ("mousedown");
	this.hitShape.removeAllEventListeners ("pressup");

	this.hitShape.on("mousedown", mousedownHandler, null, false, data);
	this.hitShape.on("pressup", pressupHandler, null, false, data);
	
	function mousedownHandler(event){
	    if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchstart") {
            // mousedownも発生する機種対策で、touchstart以外は処理しない
            return;
        }
		_this.scaleX = 0.9;
		_this.scaleY = 0.9;
	}

	var waitTimer = 0;
	function pressupHandler(event){
		if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchend") {
            // mouseupも発生する機種対策で、touchend以外は処理しない
            return;
        }
		_this.scaleX = 1;
		_this.scaleY = 1;

		if(waitTimer == 0){
			func(data);
			wait();
		}

		function wait(){
			createjs.Ticker.addEventListener('tick', tickHandler);
			function tickHandler(event){
				waitTimer++;
				if(waitTimer > 3){
					waitTimer = 0;
					createjs.Ticker.removeEventListener('tick', tickHandler);
				}
			}
		}
	}
}

//ボタンのアクティブ化
createjs.MovieClip.prototype.setButtonEnabled = function(flag){
	if(flag){
		this.mouseEnabled = true;
		var matrix =  new createjs.ColorMatrix(0,0,0,0);
		this.filters = [
		    new createjs.ColorMatrixFilter(matrix),
		];
		this.cache(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
		//this.gotoAndStop(0);
	}else{
		this.mouseEnabled = false;
		//this.gotoAndStop(4);

		var matrix =  new createjs.ColorMatrix(-100,0,0,0);
		this.filters = [
		    new createjs.ColorMatrixFilter(matrix),
		];
		this.cache(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
	}
}

//カードをボタン化する
createjs.MovieClip.prototype.addCardFunc = function(func, data){
	this.bounds = this.getBounds();
	var _this = this;

	//リスナー用のシェイプとそのヒットエリアを作る
	this.hitShape = new createjs.Shape();
	var hitAreaShape = new createjs.Shape();
	hitAreaShape.graphics.beginFill("rgba(255,0,0,1)");
	hitAreaShape.graphics.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
	this.hitShape.hitArea = hitAreaShape;
	this.addChild(this.hitShape);
	
	this.hitShape.on("mousedown", mousedownHandler, null, false, data);
	this.hitShape.on("pressup", pressupHandler, null, false, data);
	
	function mousedownHandler(event){
	    if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchstart" ) {
            // mousedownも発生する機種対策で、touchstart以外は処理しない
            return;
        }
		_this.scaleX = 0.9;
		_this.scaleY = 0.9;
	}

	var waitTimer = 0;
	function pressupHandler(event){
		if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchend" ) {
            // mouseupも発生する機種対策で、touchend以外は処理しない
            return;
        }
		_this.scaleX = 1;
		_this.scaleY = 1;

		if(waitTimer == 0){
			func(data);
			wait();
		}

		function wait(){
			createjs.Ticker.addEventListener('tick', tickHandler);
			function tickHandler(event){
				waitTimer++;
				if(waitTimer > 3){
					waitTimer = 0;
					createjs.Ticker.removeEventListener('tick', tickHandler);
				}
			}
		}
	}
}

//カードについているファンクションを変更する
createjs.MovieClip.prototype.changeCardFunc = function(func, data){

	var _this = this;
	this.hitShape.removeAllEventListeners ("mousedown");
	this.hitShape.removeAllEventListeners ("pressup");

	this.hitShape.on("mousedown", mousedownHandler, null, false, data);
	this.hitShape.on("pressup", pressupHandler, null, false, data);
	
	function mousedownHandler(event){
	    if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchstart") {
	    	return;
        }
		_this.scaleX = 0.9;
		_this.scaleY = 0.9;
	}

	var waitTimer = 0;
	function pressupHandler(event){

		if(createjs.Touch.isSupported() && event.nativeEvent.type != "touchend") {
            // mouseupも発生する機種対策で、touchend以外は処理しない
            return;
        }
		_this.scaleX = 1;
		_this.scaleY = 1;

		if(waitTimer == 0){
			func(data);
			wait();
		}
		
		function wait(){
			createjs.Ticker.addEventListener('tick', tickHandler);
			function tickHandler(event){
				waitTimer++;
				if(waitTimer > 3){
					waitTimer = 0;
					createjs.Ticker.removeEventListener('tick', tickHandler);
				}
			}
		}
	}
}



//カードのアクティブ化
createjs.MovieClip.prototype.setCardEnabled = function(flag){
	if(flag){
		this.mouseEnabled = true;
	}else{
		this.mouseEnabled = false;
	}
}
