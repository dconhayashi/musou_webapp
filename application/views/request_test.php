<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
?>
<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF8">
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta name="robots" content="noindex">
<meta http-equiv="cache-control" content="no-cache" />
<meta name="apple-touch-fullscreen" content="YES" />
<meta name="apple-mobile-web-app-capable" content="yes" />

<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<script src="https://code.jquery.com/jquery-3.1.1.min.js"  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/ja.js"></script>
<script src="/games/lib/cryptojs/aes.js"></script>
<script src="../script/jquery.marquee.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

<title>データ送信テスト</title>
</head>
<script>
  $(document).ready(function() {
      $('#btn_go').click(function() {
          $('#form2').submit();
      });
  });

  $(document).ready(function() {
      $('#btn_encrypt').click(function() {
        var data ={};
        data.redirect_url = $("#redirect_url").val();
        data.limit_medals = $("#limit_medals").val();
        data.limit_retry  = $("#limit_retry").val();
        data.limit_times  = $("#limit_times").val();
        data.event_type   = $("#event_type").val();
        data.pleasant_medals = $("#pleasant_medals").val();
        data.param1       = $("#param1").val();
        data.param2       = $("#param2").val();
        data.param3       = $("#param3").val();
        data.param4       = $("#param4").val();
        data.param5       = $("#param5").val();

        $.ajax({
            url: "/test/encrypt/",
            type:"POST",
            datatype: "json",
            cache: false,
            data: data,
            timeout:20000,

        }).done(function(data, textStatus, jqXHR){
          $("#encypted_text").val(data);
        }).fail(function(data, textStatus, errorThrown){

        }).always(function(data, textStatus, returnedObject){

        });
      });
  });
</script>
<body>
<div id="wrapper">
  <div id="page-content-wrapper">
    <h1>
    ゲーム起動テスト   
    </h1>
    <div class="col-sm-6">
      <div class="form-group">
        <form id="form1">
          <label for="contents">redirect_url:</label>
          <input type="text" class="form-control" id="redirect_url" value="/test/recieve_test">
          
          <label for="contents">limit_medals(メダル獲得最大数):</label>
          <input type="text" class="form-control" id="limit_medals" value="1000">

          <label for="contents">limit_times(プレイ回数):</label>
          <input type="text" class="form-control" id="limit_times" value="10"">

          <label for="contents">limit_retry(リプレイ回数):</label>
          <input type="text" class="form-control" id="limit_retry" value="3">

          <label for="contents">event_type:</label>
          <input type="text" class="form-control" id="event_type" value="1">

          <label for="contents">pleasant_medals(終了時特殊演出しきい値):</label>
          <input type="text" class="form-control" id="pleasant_medals" value="500">

          <label for="contents">param1:</label>
          <input type="text" class="form-control" id="param1" value="aaaaaa">

          <label for="contents">param2:</label>
          <input type="text" class="form-control" id="param2" value="bbbbb">

          <label for="contents">param3:</label>
          <input type="text" class="form-control" id="param3" value="ccccc">

          <label for="contents">param4:</label>
          <input type="text" class="form-control" id="param4" value="ddddd">

          <label for="contents">param5:</label>
          <input type="text" class="form-control" id="param5" value="eeeee">
        </form>
        <form id="form2" method="get" action="<?php echo base_url() ?>games/001_slot/main.html">
          <label for="contents">暗号化文字列:</label>
          <textarea class="form-control" rows="5" name="a" id="encypted_text"></textarea>
        </form>
      </div>
      <button type="button" class="btn btn-primary" id="btn_encrypt">暗号化</button>
      <button type="button" class="btn btn-primary" id="btn_go">URL遷移</button>
    </div>
  </div>
</div>
</body>
</html>
