<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
?>
<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF8">
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta name="robots" content="noindex">
<meta http-equiv="cache-control" content="no-cache" />
<meta name="apple-touch-fullscreen" content="YES" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<style>
  body{
    margin: 0;
    padding: 0;
  }
  iframe {
    width: 100%;
    border:none;
    margin:0;
    padding: 0;
  }
  .iframe-wrapper {
    width: 100%;
    background-color:#ccc;
    margin: 0;
    padding: 0;
  }

  #adl1 {
	width:100%;
    position: relative;
    z-index: 10;
  }
</style>

<script type="text/javascript">
$(window).on("load", function(){
	$(document).ready(function(){
		var w = <?php echo $game_data['width'] ?>;
    var h = <?php echo $game_data['height'] ?>;
		var iw = window.innerWidth, ih=window.innerHeight;
		$('iframe').height(iw * h/w);
		$('.iframe-wrapper').height(iw * h/w);
		if($('iframe').height() > ih){
			$('iframe').height(ih);
			$('.iframe-wrapper').height(ih);
		}
		$('#game').attr("src", "../games/<?php echo $game_data['directory'].'/'.$game_data['index_html'] ?>");
	});
});
</script>


<title><?php echo $game_data['title'] ?></title>
</head>


<!--背景色変更-->
<body bgcolor="#d3a613">

<!--
Adstir ダイスコネクティング unei_hokoku4 cm1 
-->
<div id="adl1">
<script type="text/javascript">
var adstir_vars = {
  ver: "4.0",
  app_id: "<?php echo $page_data['param_0'] ?>",
  ad_spot: "<?php echo $page_data['param_2'] ?>",
  center: false
};
</script>
<script type="text/javascript" src="https://js.ad-stir.com/js/adstir.js"></script>
</div>


<div>
	<div class="iframe-wrapper">
	<iframe id='game' src=""></iframe>
	</div>
</div>


<div id="adl1">
<script type="text/javascript">
var adstir_vars = {
  ver: "4.0",
  app_id: "<?php echo $page_data['param_0'] ?>",
  ad_spot: "<?php echo $page_data['param_3'] ?>",
  center: false
};
</script>
<script type="text/javascript" src="https://js.ad-stir.com/js/adstir.js"></script>
</div>

<!--
Begin:Adstir ダイスコネクティング unei_hokoku4 cm4 インタースティシャル
-->
<script type="text/javascript">
var adstir_vars = {
  ver: "4.0",
  type: "interstitial",
  app_id: "<?php echo $page_data['param_0'] ?>",
  ad_spot: "<?php echo $page_data['param_1'] ?>"
};
</script>
<script type="text/javascript" src="https://js.ad-stir.com/js/adstir.js"></script>




</body>
</html>
