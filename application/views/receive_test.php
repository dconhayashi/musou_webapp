<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
?>
<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF8">
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta name="robots" content="noindex">
<meta http-equiv="cache-control" content="no-cache" />
<meta name="apple-touch-fullscreen" content="YES" />
<meta name="apple-mobile-web-app-capable" content="yes" />

<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<script src="https://code.jquery.com/jquery-3.1.1.min.js"  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/ja.js"></script>
<script src="/games/lib/cryptojs/aes.js"></script>
<script src="../script/jquery.marquee.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

<title>データ受信テスト</title>
</head>
<script>
  $(document).ready(function() {
      $('#btn_decrypt').click(function() {
        var data ={};
        data.received_text = $("#received_text").val();
        console.log(data);
        $.ajax({
            url: "/test/decrypt/",
            type:"POST",
            datatype: "text",
            cache: false,
            data: data,
            timeout:20000,

        }).done(function(data, textStatus, jqXHR){
          $("#decypted_text").val(data);
        }).fail(function(data, textStatus, errorThrown){

        }).always(function(data, textStatus, returnedObject){

        });
      });
  });
</script>
<body>
<div id="wrapper">
  <div id="page-content-wrapper">
    <h1>
    データ受信テスト
    </h1>
    <div class="col-sm-6">
      <div class="form-group">
        <form id="form１1">
          <label for="contents">受信データ:</label>
          <textarea class="form-control" rows="10" name="a" id="received_text"><?php echo $b ?></textarea>
        </form>
        <form id="form2">
          <label for="contents">復号データ:</label>
          <textarea class="form-control" rows="10" name="a" id="decypted_text"></textarea>
        </form>
      </div>
      <button type="button" class="btn btn-primary" id="btn_decrypt">復号化</button>
    </div>
  </div>
</div>
</body>
</html>
