<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
?>
<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF8">
<meta http-equiv="X-UA-Compatible" content="IE=9">
<meta name="robots" content="noindex">
<meta http-equiv="cache-control" content="no-cache" />
<meta name="apple-touch-fullscreen" content="YES" />
<meta name="apple-mobile-web-app-capable" content="yes" />

<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<script src="https://code.jquery.com/jquery-3.1.1.min.js"  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="  crossorigin="anonymous"></script>
<script src="../script/jquery.marquee.js"></script>
<link rel="stylesheet" type="text/css" href="/css/common.css" media="all">

<style>
  body{
    background-color: #000;
  }
  .header {
  	background-color: #F00;
    height:500px;
    color: #FFF;
  }
  .hooter {
    background-color: #F00;
    height:500px;
    color: #FFF;
  }
  iframe {
    width: 100%;
    border:none;
    margin:0;
    padding: 0;
  }
  .iframe-wrapper {
    width: 100%;

    background-color:#ccc;
    margin: 0;
    padding: 0;
  }
.marquee {
  background: #000;
  width: 100%;
  overflow: hidden;
  color :#FFF;
}
</style>

<script type="text/javascript">
$(window).on("load", function(){
	$(document).ready(function(){
		var w = <?php echo $game_data['width'] ?>;
    var h = <?php echo $game_data['height'] ?>;
		var iw = window.innerWidth, ih=window.innerHeight;
		$('iframe').height(iw * h/w);
		$('.iframe-wrapper').height(iw * h/w);
		if($('iframe').height() > ih){
			$('iframe').height(ih);
			$('.iframe-wrapper').height(ih);
		}
    $('#game').attr("src", "../games/<?php echo $game_data['directory'].'/'.$game_data['index_html'] ?>");
  });

});
</script>

<script>
$(document).ready(function() {
  $('.marquee').marquee({
    direction: 'left',
    duration: 15000,
  });
});

</script>

<title><?php echo $game_data['title'] ?></title>
</head>


<!--背景色変更-->
<body bgcolor="#000">
<div class="header">サイトヘッダー</div>
<div class="marquee">キャンペーン情報　あああああああああああああああああああああああああああああああああああ</div>

<div class="iframe-wrapper">
	<iframe id='game' src=""></iframe>
</div>
<div class="hooter">サイトフッター</div>

</body>
</html>
