<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	//復号用のsalt
	const SALT = "pjr5uVmjFXADkbMaKckNEwRQD2JtjNnk";

	function __construct()
    {
        parent::__construct();
        $this->load->database('default');
        $this->load->model('Game_model');
        $this->load->model('Page_model');
		$this->load->helper('cookie');
        $this->load->library('session');
		$this->load->library('easycrypt');		

    }

	public function index($page_id=NULL)
	{
		$this->error();
	}

	public function page($page_id=NULL)
	{
		try{
			/*
			$received_data = $this->input->post();
			$serialized_data = $received_data['a'];
			$decrypted_data = $this->easycrypt->decrypt($serialized_data, self::SALT);
			$decode_data = unserialize($decrypted_data);

			//セッションを生成
			$session = $this->session->create();
			$session->data = $decode_data;
			$session->save();
			$session_id = $session->id;
			$session_data = $this->session->find($session_id);
			//print_r($session->data->limit_retry);
			//session_id をクッキーに保存
			$cookie = array(
	                   'name'   => 'session_id',
	                   'value'  => $session_id,
	                   'expire' => '3600',
	                   'domain' => '',
	                   'path'   => '/',
	                   'prefix' => '',
	               );

			set_cookie($cookie);
			*/
			$data = array();
			$page_data = $this->Page_model->get($page_id);
				//print_r($page_data);
	
			$game_id = $page_data['game_id'];
			$game_data = $this->Game_model->get($game_id);
			//print_r($game_data);
			$data['page_data'] = $page_data;
			$data['game_data'] = $game_data;
			$templete = $page_data['templete'];

			$this->load->view($templete, $data);

		}catch(Exception $e){
			$this->error();
		}
	}

	public function error(){
		print_r('ページが取得できませんでした');
		//$this->load->view('temp_error');
	}
}
