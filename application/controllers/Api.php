<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	//キー生成に使用するパスワード 
	const PASSWORD = "P4KhnVQsjTHzm4Pk2jjF";
	//初期メダル
	const DEFAULT_MEDAL = 50;



	function __construct()
    {
        parent::__construct();
        $this->load->database('default');
        $this->load->model('Game_model');
 		$this->load->model('Timetable_model');
 		$this->load->model('Lottery_model');
 	    $this->load->library('response');
 	    $this->load->library('session');
 	    $this->load->library('exception');
 		$this->post_data = $this->input->post();
 		$this->cookie = $this->input->cookie();
    }

	public function index()
	{
		$this->error();
	}

	public function games($game_id=NULL){
		switch ($game_id) {
			case '001_slot':
				$this->slot($game_id);
				break;	
			default:
				break;
		}
	}

	//キー生成
	public function get_key(){
		$session = $this->session->find($this->cookie['session_id']);
		$salt = mcrypt_create_iv(16, MCRYPT_DEV_URANDOM);
		$hash_key = hash_pbkdf2("sha256", self::PASSWORD, $salt , 1000, 32, false);
		$session->data->hash_key = $hash_key;
		$session->save();

		$this->response->set_success(array(
			"key" => $hash_key
		));
		echo json_encode($this->response);
	}

	public function slot($game_id=NULL)
	{
		$data = array();
		$result = array();
		$play_count = 0;
		try{
			//セッションを取得
			$session = $this->session->find($this->cookie['session_id']);
			//セッションに保存されているデータを取得
			$session_data = $session->data;
			//print_r($session_data);
			//ハッシュキー
			$hash_key = $session_data->hash_key;
			//メダル上限
			$limit_medals = isset($session_data->limit_medals) ? $session_data->limit_medals : 1000;
			//送信URL
			$redirect_url = isset($session_data->redirect_url) ? $session_data->redirect_url: "http://dice-online.jp/";
			//プレイ回数
			$limit_times  = isset($session_data->limit_times)  ? $session_data->limit_times : 2;
			//リトライ上限
			$limit_retry  = isset($session_data->limit_retry)  ? $session_data->limit_retry : 2;
			//初期メダル
			$default_medal = 50;

			//プレイ回数
			if(isset($session_data->play_count)){
				$play_count = $session_data->play_count + 1;
			}else{
				$play_count = 1;
			}

			$session_data->play_count = $play_count;
			$session->save();

			//残り回数 0
			$retry_left = $limit_retry + 1 - $play_count; 
			if($retry_left < 0){
				$this->response->set_error('50206', '本日の回数上限を超えています');
				echo json_encode($this->response);
				return;
			}

			//geme_idを取得
			$game_data = $this->Game_model->get($game_id);
			//現在のタイムテーブルからgroup_idを取得
			$group_id =  $this->Timetable_model->getGroupId($game_id);
			//game_id と　group_id から抽選テーブルを取得
			$lottery_data = $this->Lottery_model->get($game_data['lottery_table'], $group_id);
			$hazure_id = $this->Lottery_model->getHazureId($game_data['lottery_table'], $group_id);
			

			//$encryptHash = hash_pbkdf2("sha256", "0000", "secret", 1000, 32, true);
			$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length("aes-256-cbc"));
			$encrypted_o = openssl_encrypt("hello! this is a test!", "aes-256-cbc", $hash_key, 0, $iv);
			$encrypted = base64_encode($encrypted_o.":".bin2hex($iv));

			//抽選
			$this->response->set_success(array(
				'result_array' 	=> $this->lot($lottery_data, $limit_times, $limit_medals, $hazure_id),
				'medal' 		=> $default_medal,
				'retry_left' 	=> $retry_left,
				'game_left'		=> $limit_times,
				'regist_url'	=> $redirect_url,
				'hash_key'      => $hash_key,
				'encrypted'		=> $encrypted,
				'encrypted_o'		=> $encrypted_o,
			));
			echo json_encode($this->response);
		}catch(Exception $e){
			$this->response->set_unknown_error();
			echo json_encode($this->response);
		}
	}

	//抽選
	private function lot($lottery_data, $count, $medal_max, $hazure_id){
		//カウント
		$counter = 0;

		//メダル合計
		$medal_sum = 0;
		//分母
		$denominator = 0;

		//結果配列
		$result = [];
		$result_array = [];

		foreach ($lottery_data as $value) {
			$denominator += $value['numerater'];
		}

		while ( $counter < $count) {
			$rand = mt_rand(1,$denominator);
			$sum = 0;
			$prize_id = 0;
			foreach ($lottery_data as $value) {
				$sum += $value['numerater'];
				if($rand <= $sum){
					$prize_id =  $value['prize_id'];
					$payout = $value['payout'];
					$line = mt_rand(1,5);
					//上限を超えている場合はハズレにする
					if($medal_sum + $payout > $medal_max){
						$result = array('yaku'=>$hazure_id, 'medal'=>'0', 'line' => $line, 'replay'=>'0');
					}else{
						$medal_sum += $value['payout'];
						$result = array('yaku'=>$value['prize_id'], 'medal'=>$value['payout'], 'line' => $line, 'replay'=>$value['replay']);
					}
					//リプレイの場合はもう一回
					if($value['replay'] != 1) $counter++;
					break;
				}
			}
			$result_array[] = $result;
		}
		//echo $medal_sum;
		//配列をシャッフル
		shuffle($result_array);
		return 	$result_array;
		
	}
	public function error(){
		echo('ページが取得できませんでした');
	}
}
