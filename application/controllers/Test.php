<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
  	
  	const SALT = "pjr5uVmjFXADkbMaKckNEwRQD2JtjNnk";
	
	function __construct()
    {
        parent::__construct();
  		$this->load->library('easycrypt');
    }

	public function index($page_id=NULL)
	{
		$this->error();
	}


	public function request_test($page_id=NULL)
	{
		$this->load->view('request_test');
	}


	public function recieve_test()
	{
		$data = $this->input->post();
		$this->load->view('receive_test', $data);
	}

	public function encrypt()
	{
		$data = $this->input->post();
		$encode_data = serialize($data);
		$encrypt = $this->easycrypt->encrypt($encode_data, self::SALT);
		echo($encrypt);
	}

	public function decrypt()
	{
		$data = $this->input->post();
		$received_text = $data['received_text'];
		$decrypted_data = $this->easycrypt->decrypt($received_text, self::SALT);
		$unserialized_data = unserialize($decrypted_data);
		print_r($unserialized_data);
	}
	public function error(){
		print_r('ページが取得できませんでした');
		//$this->load->view('temp_error');
	}
}
?>