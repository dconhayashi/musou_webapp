<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slot extends CI_Controller {
	//キー生成に使用するパスワード 
	const SALT = "pjr5uVmjFXADkbMaKckNEwRQD2JtjNnk";
	//初期メダル
	const DEFAULT_MEDALS = 50;

	function __construct()
    {
        parent::__construct();
        $this->load->database('default');
        $this->load->model('Game_model');
 		$this->load->model('Timetable_model');
 		$this->load->model('Lottery_model');
 	    $this->load->library('response');
 	    $this->load->library('session');
 	    $this->load->library('exception');
 	    $this->load->library('easycrypt');
 	    $this->load->helper('cookie');

    }

	public function index()
	{
		$this->error();
	}

	public function start2($game_id)
	{
		try{
			$received_data = $this->input->post();
			
			//暗号化パラメータが無い
			if(!isset($received_data['a'])){
				throw new Exception("パラメータ不足です", 5001);
			}

			$serialized_data = $received_data['a'];
			$decrypted_data = $this->easycrypt->decrypt($serialized_data, self::SALT);
			$decode_data = unserialize($decrypted_data);

			if(!isset($decode_data)){
				throw new Exception("パラメータが解析できませんでした", 5002);
			}

			$limit_medals = isset($decode_data['limit_medals']) ? $decode_data['limit_medals']: 1000;
			//送信URL
			$redirect_url = isset($decode_data['redirect_url']) ? $decode_data['redirect_url'] : "http://dice-online.jp/";
			//プレイ回数
			$limit_times  = isset($decode_data['limit_times'])  ? $decode_data['limit_times'] : 2;
			//リトライ上限
			$limit_retry  = isset($decode_data['limit_retry'])  ? $decode_data['limit_retry']: 2;
			//初期メダル
			$default_medals = self::DEFAULT_MEDALS;

			//褒めメダル
			$pleasant_medals = isset($decode_data['pleasant_medals']) ? $decode_data['pleasant_medals']: 200;

			//geme_idを取得
			$game_data = $this->Game_model->get($game_id);
			//現在のタイムテーブルからgroup_idを取得
			$group_id =  $this->Timetable_model->getGroupId($game_id);
			//game_id と　group_id から抽選テーブルを取得
			$lottery_data = $this->Lottery_model->get($game_data['lottery_table'], $group_id);
			$hazure_id = $this->Lottery_model->getHazureId($game_data['lottery_table'], $group_id);

			if(!isset($lottery_data) || !isset($hazure_id) ){
				throw new Exception("抽選データが取得できません", 5003);
			}

			//抽選結果
			$result_lot = $this->lot($lottery_data, $limit_times, $limit_medals, $hazure_id);
			$result_array = $result_lot['result_array'];
			$medal_sum = $result_lot['medal_sum'];

			if(!isset($result_array) || !isset($medal_sum) ){
				throw new Exception("抽選に失敗しました", 5004);
			}

			$data = array(
				'medals' => $medal_sum,
				'param1' => $decode_data['param1'],
				'param2' => $decode_data['param2'],
				'param3' => $decode_data['param3'],
				'param4' => $decode_data['param4'],
				'param5' => $decode_data['param5'],
				);
			$serialized_data = serialize($data);
			$encrypted_data = $this->easycrypt->encrypt($serialized_data, self::SALT);
			
			if(!isset($encrypted_data)){
				throw new Exception("暗号化に失敗しました", 5005);
			}


			//リプレイ回数を減算
			$play_count = 0;
			$cookie_play_count = get_cookie('play_count');
			//echo $cookie_play_count;
			//return;
			//
			if(isset($cookie_play_count)){
				$play_count = $cookie_play_count + 1;
			}else{
				$play_count = 1;
			}

			$retry_left = $limit_retry + 1 - $play_count;


			if($retry_left < 0){
				//throw new Exception("プレイ回数上限オーバーです", 5006);
			}

			$cookie = array(
		                   'name'   => 'play_count',
		                   'value'  => $play_count,
		                   'expire' => '60',
		                   'domain' => '',
		                   'path'   => '/',
		                   'prefix' => '',
		               );
			set_cookie($cookie);

			//送信データ
			$this->response->set_success(array(
				'result_array' 	=> $result_array,
				'default_medals'=> $default_medals,
				'game_left'		=> $limit_times,
				'regist_url'	=> $redirect_url,
				'limit_retry'	=> $limit_retry,
				'pleasant_medals' => $pleasant_medals,
				'b'				=> $encrypted_data,
				//'retry_left' 	=> $retry_left,
			));

			echo json_encode($this->response);
		}catch(Exception $e){
			$error_message = $e->getMessage();
			$error_type = $e->getCode();
			//$this->response->set_unknown_error();
			log_message('error', $error_type.":".$error_message);
			$this->response->set_error($error_type, $error_message);
			echo json_encode($this->response);
		}
	}

	public function start($game_id=NULL)
	{
		$data = array();
		$result = array();
		$play_count = 0;
		try{
			$cookie = $this->input->cookie();
			//セッションを取得
			$session = $this->session->find($cookie['session_id']);
			//セッションに保存されているデータを取得
			$session_data = $session->data;
			//print_r($session_data);
			//メダル上限
			//print_r($session_data);
			$limit_medals = isset($session_data['limit_medals']) ? $session_data['limit_medals']: 1000;
			//送信URL
			$redirect_url = isset($session_data['redirect_url']) ? $session_data['redirect_url'] : "http://dice-online.jp/";
			//プレイ回数
			$limit_times  = isset($session_data['limit_times'])  ? $session_data['limit_times'] : 2;
			//リトライ上限
			$limit_retry  = isset($session_data['limit_retry'])  ? $session_data['limit_retry']: 2;
			//初期メダル
			$default_medals = self::DEFAULT_MEDALS;
			//print_r($session_data);
			//print_r($session_data['play_count']);
		
			//プレイ回数
			if(isset($session_data['play_count'])){
				$play_count = $session_data['play_count'];
			}
			$play_count++;
			$session->data['play_count'] = $play_count;
			$session->save();
			//print_r($session_data);
			//残り回数 0
			/*
			$retry_left = $limit_retry + 1 - $play_count; 
			if($retry_left < 0){
				$this->response->set_error('50206', '本日の回数上限を超えています');
				echo json_encode($this->response);
				return;
			}
			*/
			//geme_idを取得
			$game_data = $this->Game_model->get($game_id);
			//現在のタイムテーブルからgroup_idを取得
			$group_id =  $this->Timetable_model->getGroupId($game_id);
			//game_id と　group_id から抽選テーブルを取得
			$lottery_data = $this->Lottery_model->get($game_data['lottery_table'], $group_id);
			$hazure_id = $this->Lottery_model->getHazureId($game_data['lottery_table'], $group_id);

			//抽選結果
			$result_lot = $this->lot($lottery_data, $limit_times, $limit_medals, $hazure_id);
			$result_array = $result_lot['result_array'];
			$medal_sum = $result_lot['medal_sum'];

			$data = array(
				'medals' => $medal_sum,
				'frm' 	 => '',
				'param1' => $session_data['param1'],
				'param2' => $session_data['param2'],
				'param3' => $session_data['param3'],
				'param4' => $session_data['param4'],
				'param5' => $session_data['param5'],
				);
			$serialized_data = serialize($data);
			$encrypted_data = $this->easycrypt->encrypt($serialized_data, self::SALT);
			//print_r($unserialized_data);
			//抽選
			$this->response->set_success(array(
				'result_array' 	=> $result_array,
				'default_medals'=> $default_medals,
				'retry_left' 	=> $retry_left,
				'limit_retry'   => $limit_retry,
				'game_left'		=> $limit_times,
				'regist_url'	=> $redirect_url,
				'play_count'	=> 1,
				'b'				=> $encrypted_data,
			));
			echo json_encode($this->response);
		}catch(Exception $e){
			$this->response->set_unknown_error();
			echo json_encode($this->response);
		}
	}

	//抽選
	private function lot($lottery_data, $count, $medal_max, $hazure_id){
		//カウント
		$counter = 0;

		//メダル合計
		$medal_sum = self::DEFAULT_MEDALS;
		//分母
		$denominator = 0;

		//結果配列
		$result = [];
		$result_array = [];

		foreach ($lottery_data as $value) {
			$denominator += $value['numerater'];
		}

		while ( $counter < $count) {
			$rand = mt_rand(1,$denominator);
			$sum = 0;
			$prize_id = 0;
			//1ゲームに必要なメダル数
			$medal_sum -= 3;
			foreach ($lottery_data as $value) {
				$sum += $value['numerater'];
				if($rand <= $sum){
					$prize_id =  $value['prize_id'];
					$payout = $value['payout'];
					$line = mt_rand(1,5);
					//上限を超えている場合はハズレにする
					if($medal_sum + $payout> $medal_max){
						$result = array('yaku'=>$hazure_id, 'medal'=>'0', 'line'=>$line, 'replay'=>'0');
					}else{
						$medal_sum += $payout;
						$result = array('yaku'=>$value['prize_id'], 'medal'=>$value['payout'], 'line' => $line, 'replay'=>$value['replay']);
					}
					//リプレイの場合はもう一回
					if($value['replay'] == 1){
						$medal_sum += 3;
					}else{
						$counter++;
					}
					break;
				}
			}
			$result_array[] = $result;
		}
		//echo $medal_sum;
		//配列をシャッフル
		shuffle($result_array);
		$data = array(
				'medal_sum' => $medal_sum,
				'result_array' => $result_array,
			);
		return $data;
	}
	public function error(){
		echo('ページが取得できませんでした');
	}
}
