<?php
class Page_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }

	//ページデータを取得
	function get($page_id = null)
	{
		$query = $this->db->order_by('id', 'DESC');
		$query = $this->db->limit(1);
		
		if($page_id){
			$query = $this->db->where('page_id', $page_id);
		}
		$query = $this->db->get('page');
		
		if($query->num_rows() > 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
}
?>