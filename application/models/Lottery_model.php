<?php
class Lottery_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }

	//リストを取得
	function get($table=null, $group_id=1)
	{	
		$cached_data = $this->cache->get("Lottery_model_get_{$table}_{$group_id}");
		if($cached_data){
			return $cached_data;
		}else{
			$query = $this->db->where('group_id', $group_id);
			$query = $this->db->get($table);
			
			if($query->num_rows() > 0){
				$result = $query->result_array();
				$this->cache->save("lottery_model_get_{$table}_{$group_id}", $result, 600);
				return $result;
			}else{
				return false;
			}
		}
	}

	//ハズレ役を取得
	function getHazureId($table=null, $group_id=1)
	{	
		//$this->cache->delete("Lottery_model_getHazureId_{$table}_{$group_id}");
		$cached_data = $this->cache->get("Lottery_model_getHazureId_{$table}_{$group_id}");
		if($cached_data){
			return $cached_data;
		}else{
			$query = $this->db->where('group_id', $group_id);
			$query = $this->db->where('blank', 1);
			$query = $this->db->select('prize_id');
			$query = $this->db->limit(1);
			$query = $this->db->get($table);
			
			if($query->num_rows() > 0){
				$row = $query->row();
				$hazure_id = $row->prize_id;
				$this->cache->save("Lottery_model_getHazureId_{$table}_{$group_id}", $hazure_id, 600);
				return $hazure_id;
			}else{
				return false;
			}
		}
	}
}
?>