<?php
class Timetable_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }

	//リストを取得
	function getGroupId($game_id = null)
	{
		$now = date("Y-m-d H:i:s");
		$query = $this->db->where('open_time <', $now);
		$query = $this->db->where('close_time >', $now);
		$query = $this->db->order_by('open_time', 'DESC');
		$query = $this->db->limit(1);
		
		if($game_id){
			$query = $this->db->where('game_id', $game_id);
		}
		$query = $this->db->get('timetable');
		
		if($query->num_rows() > 0){
			return $query->row('group_id');
		}else{
			return false;
		}
	}
}
?>