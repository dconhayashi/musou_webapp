<?php
class Game_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
        $this->load->driver('cache', array('adapter' => 'memcached'));
    }

	//リストを取得
	function get($game_id = null)
	{
	
		$cached_data = $this->cache->get("Game_model_get_{$game_id}");
		if($cached_data){
			return $cached_data;
		}else{
			$now = date("Y-m-d H:i:s");
			//$query = $this->db->where('open_time <', $now);
			//$query = $this->db->where('close_time >', $now);
			//$query = $this->db->order_by('open_time', 'DESC');
			$query = $this->db->limit(1);			
			$query = $this->db->where('game_id', $game_id);
			$query = $this->db->get('game');
			
			if($query->num_rows() > 0){
				$result =  $query->row_array();
				$this->cache->save("Game_model_get_{$game_id}", $result, 600);
				return $result;
			}else{
				return false;
			}
		}
	}
}
?>