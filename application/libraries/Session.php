<?php

//namespace Ecotech\SMG;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * セッションクラス
 */
class Session {

    /**
     * セッション有効時間
     */
    const TTL = 3600; // 1時間

    /**
     * セッションID
     * @var string
     */

    public $id;

    /**
     * セッションデータ
     * @var array
     */
    public $data;

    function __construct() {
        $this->id = null;
        $this->data = array();
    }

    /**
     * セッションIDからセッションインスタンス取得
     *
     * @param string $id プレイID
     * @return \App\Model\Session
     */
    public static function find($id) {
        $data = get_instance()->cache->memcached->get($id);
        if (!$data) {
            return false;
        }

        $session = new static();
        $session->id = $id;
        $session->data = $data;

        return $session;
    }

    /**
     * セッションの作成
     *
     * @return \self
     */
    public static function create() {
        $session = new self();
        $session->id = date("Ymd") . md5(uniqid(microtime(), 1)) . getmypid();
        $session->data = array();

        return $session;
    }

    public function save() {
        $CI = & get_instance();

        return $CI->cache->memcached->save($this->id, $this->data, self::TTL);
    }

    /**
     * getter
     *
     * @param string $key セッションデータキー
     * @return mixed
     */
    public function __get($key) {
        if ($key == 'id') {
            return $this->id;
        }

        if (isset($this->data[$key])) {
            return $this->data[$key];
        }

        return null;
    }

    /**
     * setter
     *
     * @param string $key セッションデータキー
     * @param mixed $value セッションデータ値
     * @return	void
     */
    public function __set($key, $value) {
        $this->data[$key] = $value;
    }

    /**
     * セッションの破棄
     *
     * @return boolean
     */
    public function delete() {
        if (!$this->id) {
            return false;
        }

        return get_instance()->cache->memcached->delete($this->id);
    }

    /**
     * セッション中の鍵情報をクリアする
     *
     * 共通鍵と有効期限をクリアする
     */
    public function clear_key() {
        $this->data['key'] = null;
        $this->data['key_expiration'] = null;
    }

}
?>