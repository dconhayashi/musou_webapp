<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * APIレスポンスクラス
 */
class Response implements \JsonSerializable {

    private $meta;
    private $data;

    /**
     * APIレスポンスクラス
     */
    public function __construct() {
        $this->clear();
        $this->set_unknown_error();
    }

    /**
     * レスポンス内容クリア
     */
    public function clear() {
        $this->meta = new \stdClass();
        $this->data = null;
    }

    /**
     * jsonSerialize実装
     *
     * @return array
     */
    public function jsonSerialize() {
        if (!$this->data) {
            return array(
                'meta' => (array) $this->meta,
                'data' => null
            );
        }

        return array(
            'meta' => (array) $this->meta,
            'data' => $this->data
        );
    }

    /**
     * 成功時レスポンスデータセット
     *
     * @param mixed $data
     */
    public function set_success($data) {
        $this->clear();

        $this->meta->code = 200;
        $this->data = $data;
    }

    /**
     * エラー時レスポンス(原因不明)
     */
    public function set_unknown_error() {
        $this->clear();

        $this->meta->code = 500;
        $this->meta->error_type = "50099";
        $this->meta->error_message = "原因不明のエラーです";
        $this->data = null;
    }

    /**
     * エラー時レスポンスデータセット
     *
     * @param \Ecotech\SMG\Api\Exception $e
     */
    public function set_error($error_type, $error_message) {
        $this->clear();

        $this->meta->code = 500;
        $this->meta->error_type = $error_type;
        $this->meta->error_message = $error_message;
    }

    /**
     * メンテナンス時レスポンスデータセット
     *
     * @param \Ecotech\SMG\Api\Exception\MaintenanceException $e メンテナンス例外インスタンス
     */
    public function set_maintenance(Exception\MaintenanceException $e) {
        $this->clear();

        $this->meta->code = $e->getCode();
        $this->meta->error_type = $e->getType();
        $this->meta->error_message = $e->getMessage();

        if ($e->open_time) {
            $this->data = array(
                'open_time' => $e->open_time,
                'close_time' => $e->close_time,
                'contents' => $e->contents
            );
        } else {
            $this->data = array(
                'contents' => $e->contents
            );
        }
    }

}
