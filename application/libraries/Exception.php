<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SMGAPI例外基本クラス
 */
class Exception extends \Exception {

    /**
     * エラータイプ
     * @var string
     */
    private $type;

    /**
     * SMGAPI例外基本クラス
     *
     * @param int $code エラーコード
     * @param string $type エラータイプ
     * @param string $message エラーメッセージ
     * @param \Ecotech\SMG\Api\Exception $previous
     */
    public function __construct($code, $type, $message, \Exception $previous = null) {
        $this->type = $type;
        parent::__construct($message, $code, $previous);
    }

    /**
     * エラータイプ
     *
     * @return string
     */
    final public function getType() {
        return $this->type;
    }

}
